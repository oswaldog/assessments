<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::get('/user', 'Auth\UserController@current');

    Route::patch('settings', 'Settings\SettingsController@updateProfile');
    Route::get('settings', 'Settings\SettingsController@index');
    Route::get('settings/workteam_info', 'Settings\SettingsController@workTeamInfo');
    Route::post('settings', 'Settings\SettingsController@addTeamMembers');
    Route::put('settings', 'Settings\SettingsController@updateTeamMembers');
    Route::delete('settings/{id}', 'Settings\SettingsController@deleteTeamMembers');

    Route::get('areas', 'Areas\AreasController@index');
    Route::post('areas', 'Areas\AreasController@store');
    Route::put('areas', 'Areas\AreasController@update');
    Route::delete('areas/{id}', 'Areas\AreasController@delete');

    Route::post('export', 'MainForm\InspectionsExportController@exportAppraisalInspection');
    Route::post('appraisal', 'Appraisal\AppraisalController@updateAppraisalStatus');

    Route::get('geocoder', 'MainForm\GeocoderController@index');

    Route::get('mainform', 'MainForm\MainFormController@index');
    Route::post('mainform', 'MainForm\MainFormController@store');
    Route::put('mainform', 'MainForm\MainFormController@update');
    Route::delete('mainform', 'MainForm\MainFormController@close');

    Route::get('mainform/fetch_inspectors/{id}', 'MainForm\MainFormController@fetchInspectors');
    Route::get('mainform/requests', 'MainForm\MainFormController@appraisalRequests');
    Route::get('mainform/comparables', 'MainForm\MainFormController@appraisalComparables');
    Route::get('mainform/templates', 'MainForm\MainFormController@appraisalTemplates'); 

    Route::get('mainform/inspections/templates', 'MainForm\MainFormController@fetchTemplatesInfo'); 
    Route::post('mainform/inspections/template', 'MainForm\MainFormController@applyTemplate');
    Route::get('mainform/inspections/arrival', 'MainForm\MainFormController@fetchArrival'); 
    Route::post('mainform/inspections/arrival', 'MainForm\MainFormController@saveArrival');    
 
    Route::get('mainform/request/status', 'MainForm\MainFormController@requestsStatus');
    Route::get('mainform/request/types', 'MainForm\MainFormController@requestsTypes');
    Route::get('mainform/cities', 'MainForm\MainFormController@requestsCities');
    Route::get('mainform/sectors', 'MainForm\MainFormController@requestsSectors');
    
    Route::get('inspection', 'MainForm\InspectionsAreasController@fetchItems');
    Route::post('inspection', 'MainForm\InspectionsAreasController@store');
    Route::delete('inspection/{id}', 'MainForm\InspectionsAreasController@delete');

    Route::get('attachment', 'Attachment\AttachmentController@index');
    Route::get('attachment/comparables', 'Attachment\AttachmentController@photosComparables');
    Route::post('attachment', 'Attachment\AttachmentController@addAttachments');
    Route::put('attachment', 'Attachment\AttachmentController@updatePosition');
    Route::delete('attachment/{id}', 'Attachment\AttachmentController@deleteAttachment');

    Route::get('multiImages', 'Attachment\AttachmentController@downloadMultiImages');    
    Route::post('multiImages', 'Attachment\AttachmentController@deleteMultiImages');
    Route::get('oneOrMoreImages', 'Attachment\AttachmentController@downloadOneOrMoreImages');    
    Route::post('oneOrMoreImages', 'Attachment\AttachmentController@deleteOneOrMoreImages');

    Route::get('propertyTypes', 'PropertyTypesController@index');
    Route::get('provinces', 'LocationsController@fetchProvinces');
    Route::get('municipalities', 'LocationsController@fetchMunicipalities');
    Route::get('cities', 'LocationsController@fetchCities');
    Route::get('sectors', 'LocationsController@fetchSectors');

    Route::get('comparables/fetchRequestData','Comparables\ComparablesController@fetchRequestData');
    Route::get('comparables/fetchComparables','Comparables\ComparablesController@fetchComparables');
    // Route::get('comparables/fetchAnalysisData','Comparables\ComparablesController@fetchAnalysisData');


});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::get('register', 'Auth\RegisterController@index');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});
