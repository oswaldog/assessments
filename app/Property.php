<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model 
{

    protected $table = 'properties';
    public $timestamps = true;

    public function appraisal_request()
    {
        return $this->belongsTo('App\AppraisalRequest');
    }
    
    public function levels()
    {
        return $this->hasMany('App\Level','property_id');
    }


}