<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InspectionArrival extends Model
{
    //
    protected $table = 'inspection_arrivals';
    public $timestamps = false;
}
