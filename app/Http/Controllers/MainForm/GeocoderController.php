<?php

namespace App\Http\Controllers\MainForm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GeocoderController extends Controller
{
    //

    public function index(Request $request){
            //GeoCode Api 
            $lat    = $request->input('lat');
            $lng    = $request->input('lng');
            $region = config('services.google_maps_api.region');
            $key    = config('services.google_maps_api.key');
            $lang   = config('services.google_maps_api.locale');
            $url    = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&region=$region&language=$lang&key=$key";
            //$resp_json = file_get_contents($url);
            
            // Create a curl handle to a non-existing location
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $resp_json = '';
            $resp_json = curl_exec($ch);

            // Close handle
            curl_close($ch);


            $resp = json_decode($resp_json, true);
            if ($resp['status'] === "OK"){
                $address = $resp['results'][0]['address_components'];
                return $this->getGeoCodingAddress($address);
            } else {
                $address = $resp['status'];
                return $this->getGeoCodingStatus($address); 
            }
    }

    public function getGeoCodingStatus($elements){
        $addr = [];
        $addr['status'] = "ERROR";
        $addr['swal_text'] = "Por favor contacte al administrador del sistema";
        if ($elements === "ZERO_RESULTS"){
            $addr['swal_title'] = "Dirección no encontrada";
            $addr['swal_text'] = "Esto puede ocurrir si las coordenadas no se corresponden con ninguna dirección";
            }
        if ($elements === "OVER_DAILY_LIMIT"){
        $addr['swal_title'] = "Problemas con el API";
        }
        if ($elements === "OVER_QUERY_LIMIT"){
        $addr['swal_title'] = "Problemas con la cuota del API";
        }
        if ($elements === "REQUEST_DENIED"){
        $addr['swal_title'] = "Solicitud del API denegada";
        }
        if ($elements === "INVALID_REQUEST"){
        $addr['swal_title'] = "Solicitud del API invalida";
        }
        if ($elements === "UNKNOWN_ERROR"){
        $addr['swal_title'] = "La solicitud no pudo ser procesada";
        $addr['swal_text'] = "Por favor intente nuevamente, si el problema persiste contacte al administrador del sistema";
        }

        return $addr;
    }


    public function getGeoCodingAddress($elements){
        $addr = [];
     
            foreach($elements as $e){
                if(isset($e['types']) && in_array("street_number",$e['types'])) $addr['street_number'] = $e['long_name'];
                if(isset($e['types']) && in_array("route",$e['types'])) $addr['route'] = $e['long_name'];
                if(isset($e['types']) && in_array("sublocality_level_1",$e['types'])) $addr['sector'] = $e['long_name'];
                if(isset($e['types']) && in_array("locality",$e['types'])) $addr['city'] = $e['long_name'];
                if(isset($e['types']) && in_array("administrative_area_level_2",$e['types'])) $addr['municipality'] = $e['long_name'];
                if(isset($e['types']) && in_array("administrative_area_level_1",$e['types'])) $addr['province'] = $e['long_name'];
                if(isset($e['types']) && in_array("country",$e['types'])) $addr['country_code'] = $e['short_name'];
                if(isset($e['types']) && in_array("country",$e['types'])) $addr['country'] = $e['long_name'];
                $addr['status'] = "OK";
                $addr['swal_title'] = "Coordenada geográfica fuera de rango";
                $addr['swal_text'] = "Por favor seleccione una coordenada geográfica propia de República Dominicana";
            }
        
        return $addr;
    }

}
