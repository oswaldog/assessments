<?php

namespace App\Http\Controllers\MainForm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AppraisalArea;
use DB;

class InspectionsAreasController extends Controller
{
    //

    protected function store(Request $request)
    {  
        $appraisalRequestId = $request->input('appraisal_request_id');
        $areaId = $request->input('areaId');
        $dimension = $request->input('dimension');

        $appraisalArea = new AppraisalArea;
        $appraisalArea->appraisal_request_id = $appraisalRequestId;
        $appraisalArea->area_id = $areaId;
        $appraisalArea->dimension = $dimension;

        $appraisalArea->save();
  
    }

    protected function fetchItems(Request $request){
        $appraisalRequestId = $request->input('appraisal_request_id');
        $appraisalArea = DB::table('appraisal_areas')
                            ->where('appraisal_request_id','=',$appraisalRequestId)
                            ->join('areas','appraisal_areas.area_id','=','areas.id')
                            ->select('appraisal_areas.id', 'appraisal_areas.area_id', 'areas.name AS area', 'dimension')
                            ->get();
        return $appraisalArea;
    }


    protected function delete($idAppraisalArea)
    {
        $appraisalArea = AppraisalArea::where('id',$idAppraisalArea)->first();

        $appraisalArea->delete();  
    }
}
