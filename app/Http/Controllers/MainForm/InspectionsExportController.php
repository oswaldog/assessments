<?php

namespace App\Http\Controllers\MainForm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\InspectionsMultiSheetExport;
use Maatwebsite\Excel\Facades\Excel;

class InspectionsExportController extends Controller
{


    public function exportAppraisalInspection(Request $request)
    {
        $appraisal_request_id = $request->input('appraisal_request_id'); 
        $file_name = 'export.xlsx';
        return Excel::store(new InspectionsMultiSheetExport($appraisal_request_id), $appraisal_request_id.'/'.$file_name,'internal');
    }


}
