<?php

namespace App\Http\Controllers\MainForm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AppraisalRequest;
use App\Property;
use App\Level;
use App\Country;
use App\Province;
use App\Municipality;
use App\City;
use App\Sector;
use App\RegisterType;
use App\InspectionArrival;
use DB;


class MainFormController extends Controller
{
    //
    protected function index(Request $request){
        $id = $request->input('id');        
        switch($id){


            case 2: 
                //Appraisal Request
                $user_id = $request->input('user_id'); 
                $banks = $this->fetchBanks();
                $property_types = $this->fetchPropertyTypes();
                $property_use_status = $this->fetchPropertyUseStatus();
                $payment_methods = $this->paymentMethods();
                $delivery_types = $this->fetchDeliveryTypes();
                $inspectors = $this->fetchInspectors($user_id);
                

                return compact(['banks','property_types','property_use_status','payment_methods','delivery_types','inspectors']) ; 

            case 3: 
                //Inspection
                $solicitud_id = $request->input('solicitud_id');
                //Por medio de la solicitud_id se debe retornar property_id(int), inspeccion(object), niveles(Array)
                $inspection_data = $this->fetchInspection($solicitud_id);

                if (empty($inspection_data['niveles'])) {
                    $coating_selected = null;
                }else{
                    $coating_selected = $this->generate_coating_arrays($inspection_data['niveles']);
                }

                $coating_base = $this->coatingMaterialsBase();

                return compact('inspection_data','coating_base','coating_selected');

            case 5: 
                $inspection_date = $request->input('inspection_date');
                return $this->fetchInspectionQtyPerDate($inspection_date);

            case 6: 
                $inspector = $request->input('inspector');
                $inspection_date = $request->input('inspection_date');
                return $this->fetchInspectionQtyPerInspector($inspector,$inspection_date); 
                
            case 7: 
                $delivery_date = $request->input('delivery_date');
                return $this->fetchInspectionQtyPerDeliveryDate($delivery_date);                
        }

    }



    protected function fetchRequests($roleInfo) {
        $condition_status = [];
        $condition_role = null;
        $conditionA1 = $conditionA2 = $conditionA3 = 'appraisal_requests_view.id';
        $conditionB1 = $conditionB2 = $conditionB3 = 0;
        $operator1 = $operator2 = '<';
        $operator3 = '>';

        $ownerUserId = $roleInfo->owner_user_id;
        $memberUserId = $roleInfo->member_user_id;
        $userRole = $roleInfo->role;

        switch ($userRole):
            case 0:         //Admin
                $condition_status = [1,2,3,4];
                $condition_role = 'work_teams_view.is_administrative';
                $conditionA1 = 'appraisal_requests_view.appraisal_status_id';
                $operator1 = '=';
                $conditionB1 = 4;                
                $conditionA2 = 'appraisal_requests_view.is_paid';
                $operator2 = '=';
                $conditionB2 = false;
                $conditionA3 = 'appraisal_requests_view.inspector_id';
                $operator3 = '<>';
                $conditionB3 = $memberUserId;
                break;
            case 1:         //Inspector
                $condition_status = [1]; //'Agendada'
                $condition_role = 'work_teams_view.is_inspector';
                $conditionA3 = 'appraisal_requests_view.inspector_id';
                $operator3 = '=';
                $conditionB3 = $memberUserId;
                break;
            case 2:         //Evaluator
                $condition_status = [2,3]; //'Inspeccionada', 'Revisada'
                $condition_role = 'work_teams_view.is_evaluator';
                break;
            case 3:         //Approver
                $condition_status = [2,3,4]; //'Inspeccionada', 'Revisada', 'Evaluada'
                $condition_role = 'work_teams_view.is_approver';
                break;                                    
        endswitch;

        $qry = DB::table('appraisal_requests_view')
                ->join('work_teams_view', function ($join) use ($memberUserId, $condition_role) {
                    $join->on('appraisal_requests_view.owner_user_id', '=', 'work_teams_view.owner_user_id')
                         ->where('work_teams_view.member_user_id', '=', $memberUserId)
                         ->where($condition_role, '=', true);
                    })
                ->where($conditionA3,$operator3,$conditionB3)
                ->where('register_type_id','=',1)                    
                ->Where(function($query1) use ($condition_status,$conditionA1,$operator1,$conditionB1,$conditionA2,$operator2,$conditionB2) {
                    return $query1
                        ->whereIn('appraisal_status_id',$condition_status)
                        ->orWhere(function($query2) use ($conditionA1,$operator1,$conditionB1,$conditionA2,$operator2,$conditionB2) {
                        return $query2
                            ->where($conditionA1,$operator1,$conditionB1)
                            ->where($conditionA2,$operator2,$conditionB2);
                        });
                })                 
                ->select(
                'appraisal_requests_view.id',
                'appraisal_requests_view.owner_user_id',
                'is_administrative',
                'is_inspector',
                'is_evaluator',
                'is_approver',
                'register_type_id',
                'register_type_name',
                'applicant_name',
                'applicant_phone',
                'applicant_email',
                'contact_name',
                'contact_phone',
                'delivery_type_id',
                'delivery_type_name',
                'bank_id',
                'bank_name',
                'amount',
                'payment_method_id',
                'payment_method_name',
                'is_paid',
                'inspection_date',
                'inspection_time',
                'delivery_date',
                'inspector_id',
                'inspector_name',
                'appraisal_status_id',
                'appraisal_status_name',
                'address',
                'comments',
                'created_by_user_id',
                'updated_by_user_id',
                'appraisal_request_id',
                'property_id',
                'property_type_id',
                'property_type_name',
                'property_use_status_id',
                'property_use_status_name',
                'property_number',
                'property_levels',
                'construction_year',
                'geo_coordinates',
                'cadastral_number',
                'registration_number',
                'book_number',
                'folio_number',
                'expedition_date',
                'is_new',
                'owner',
                'land_area',
                'improvement_area',
                'sale_date',
                'sale_amount',
                'appraisal_amount',
                'offer_amount',
                'sale_type_id',                
                'residence',
                'address_number',
                'address_street',
                'sector_id',
                'sector_name',
                'city_id',
                'city_name',
                'province_id',
                'province_name',
                'municipality_id',
                'municipality_name',
                'country_id',
                'country_name')
                ->get();

        return $qry;        
    }


    protected function fetchRequestStatus(){
        $qry = DB::table('appraisal_status')
        ->select('id','name')
        ->get();
    return $qry;
    }

    protected function fetchInspection($solicitud_id) {

        $inspeccion = Property::where('appraisal_request_id', $solicitud_id)
                                ->select('id',
                                'appraisal_request_id',
                                'property_type_id',
                                'property_levels',
                                'property_levels',
                                'general_data AS data',
                                )
                                ->get();
        $niveles = array();
        if (count($inspeccion)) {
            $property_id = $inspeccion[0]->id;
            $inspeccion_levels = $inspeccion[0]->property_levels;
            $nivelesCollection = Level::where('property_id', $property_id)->select('internal_data AS data')->orderBy('level')->get();

            foreach ($nivelesCollection as $item) {
                array_push($niveles, json_decode($item->data));
            }

            $inspeccion = json_decode($inspeccion[0]->data);

        }

        //dd($property_id,$inspeccion,$niveles,$inspeccion_levels);
        /**
         * property_id = 'properties.id', Never is empty
         * inspeccion = 'properties.general_data'. First time is null
         * niveles = 'levels.internal_data'. First time is empty
         * inspeccion_levels = 'properties.property_levels', Never is empty
         */

        return compact(['property_id','inspeccion','niveles','inspeccion_levels']);

    }

    protected function fetchBanks() {
        $qry = DB::table('banks')
            ->select('id','name')
            ->where('id','>',0)
            ->get();
        return $qry;
    }
    
    protected function fetchPropertyTypes() {
        $qry = DB::table('property_types')
            ->select('id','name')
            ->get();
        return $qry;        
    }

    protected function fetchPropertyUseStatus(){
        $qry = DB::table('property_use_status')
            ->select('id','name')
            ->get();
        return $qry;
    }

    protected function paymentMethods(){
        $qry = DB::table('payment_methods')
            ->select('id','name')
            ->get();
        return $qry;
    }

    protected function fetchDeliveryTypes(){
        $qry = DB::table('delivery_types')
            ->select('id','name')
            ->get();
        return $qry;
    }

    protected function fetchInspectors($user_id){

        $qry = DB::table('work_teams_view')
                ->select('member_user_id AS id','member_user_name AS name')
                ->where('is_inspector','=',true)
                ->whereIn('owner_user_id', function($query) use ($user_id){
                    $query->select('owner_user_id')
                    ->from('work_teams_view')
                    ->where('member_user_id','=',$user_id)
                    ->where('is_administrative','=',true);
                })
                ->get();


        // $qry = DB::table('users')
        //     ->select('id','name')
        //     ->orderBy('name','asc')
        //     ->get();

        return $qry;
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {           

        /**
         * Control por medio de "spyForm" del modelo a afectar (1=Solicitudes o 2=Inspecciones) 
         **/     


        if ($request->input('spyForm') == 1) {

            //dd($request);

            $country = Country::firstOrCreate(['name' => $request->input('country')]);
            $province = Province::firstOrCreate(['name' => $request->input('province'), 'country_id' => $country->id]);
            $municipality = Municipality::firstOrCreate(['name' => $request->input('municipality'), 'province_id' => $province->id]);
            $city = City::firstOrCreate(['name' => $request->input('city'), 'municipality_id' => $municipality->id]);
            $sector = Sector::firstOrCreate(['name' => $request->input('sector'), 'city_id' => $city->id]);

            $requestModel = new AppraisalRequest;
            $propertyModel = new Property;

            //solicitante
            $requestModel->register_type_id = $request->input('register_type');
            $requestModel->applicant_name = $request->input('applicant_name');
            $requestModel->applicant_phone = $request->input('applicant_phone');
            $requestModel->applicant_email = $request->input('applicant_email');
            $requestModel->contact_name = $request->input('contact_name');
            $requestModel->contact_phone = $request->input('contact_phone');
            $requestModel->delivery_type_id = $request->input('delivery_type');
            $requestModel->bank_id = $request->input('bank');
            $propertyModel->property_type_id = $request->input('property_type');
            $propertyModel->is_new = $request->input('property_condition');
            $propertyModel->property_use_status_id = $request->input('property_use_status');
            $requestModel->amount = $request->input('amount');
            $requestModel->payment_method_id = $request->input('payment_method');            
            $requestModel->is_paid = $request->input('status_pago');
            $requestModel->comments = $request->input('comments');
            $requestModel->owner_user_id = $request->input('owner_user_id');

            //agenda
            $requestModel->address = $request->input('address');
            $propertyModel->property_number = $request->input('property_number');
            $propertyModel->property_levels = $request->input('property_levels');
            $propertyModel->construction_year = $request->input('construction_year');
            $requestModel->inspector_id = $request->input('inspector');
            $requestModel->inspection_date = $request->input('inspection_date');
            $requestModel->inspection_time = $request->input('inspection_time');
            $requestModel->delivery_date = $request->input('delivery_date');
            $propertyModel->geo_coordinates = $request->input('geocoords');
            $propertyModel->residence = $request->input('residence');
            $propertyModel->address_number = $request->input('numero');
            $propertyModel->address_street = $request->input('calle');

            //informacion legal
            $propertyModel->cadastral_number = $request->input('catastro');
            // $propertyModel->registration_number = $request->input('registration_number');
            // $propertyModel->book_number = $request->input('book_number');
            // $propertyModel->folio_number = $request->input('folio_number');
            // $propertyModel->expedition_date = $request->input('expedition_date');
            // $propertyModel->owner = $request->input('property_owner');
            // $propertyModel->land_area = (is_null($request->input('property_land_area'))) ? 0 : $request->input('property_land_area');
            // $propertyModel->improvement_area = (is_null($request->input('property_improvement_area'))) ? 0 : $request->input('property_improvement_area');
           

            $propertyModel->sector_id = $sector->id;
            $requestModel->created_by_user_id = $request->input('user_id');

            $requestModel->appraisal_status_id = 1;


            DB::transaction(function() use ($requestModel,$propertyModel) {
                $requestModel->save();
                AppraisalRequest::find($requestModel->id)->property()->save($propertyModel);
             });



        }
        if ($request->input('spyForm') == 2) {

            //LAS INSPECCIONES SERAN SOLO ACTUALIZACIONES

            $inspeccion = New Inspeccion;

            $inspeccion->solicitud_id = $request->solicitud_id;
            $inspeccion->niveles = count($request->niveles);
            $inspeccion->data = json_encode($request->inspeccion);

            $inspeccion->save();

            $pos = 0;
            foreach ($request->niveles as $arrNivel) {
                $niveles = New Niveles;
                $niveles->inspeccion_id = $inspeccion->id;
                $niveles->nivel = $pos + 1;
                $niveles->data = json_encode($arrNivel);
                $niveles->save();
                ++$pos;
            }

            return $inspeccion->id;            
            
        }
    }
        
    
    protected function update(Request $request)
    {   

        if ($request->input('spyForm') == 1) {

            
            $country = Country::firstOrCreate(['name' => $request->input('country')]);
            $province = Province::firstOrCreate(['name' => $request->input('province'), 'country_id' => $country->id]);
            $municipality = Municipality::firstOrCreate(['name' => $request->input('municipality'), 'province_id' => $province->id]);
            $city = City::firstOrCreate(['name' => $request->input('city'), 'municipality_id' => $municipality->id]);
            $sector = Sector::firstOrCreate(['name' => $request->input('sector'), 'city_id' => $city->id]);

            $update_property_register = [
                'cadastral_number' => $request->input('catastro'),
                // 'registration_number' => $request->input('registration_number'),                
                // 'book_number' => $request->input('book_number'),  
                // 'folio_number' => $request->input('folio_number'),                              
                // 'expedition_date' => $request->input('expedition_date'),
                // 'owner' => $request->input('property_owner'),     
                // 'land_area' => $request->input('property_land_area'),
                // 'improvement_area' => $request->input('property_improvement_area'),                 

                'address_number' => $request->input('numero'),
                'address_street' => $request->input('calle'),
                'geo_coordinates' => $request->input('geocoords'),
                'is_new' => $request->input('property_condition'),
                'construction_year' => $request->input('construction_year'),
                'property_levels' => $request->input('property_levels'),
                'property_number' => $request->input('property_number'),
                'property_type_id' => $request->input('property_type'),
                'property_use_status_id' => $request->input('property_use_status'),
                'residence' => $request->input('residence'),
                'sector_id' => $sector->id
            ];
            
            
            
            $update_appraisal_request_register = [
                'address' => $request->input('address'),
                'amount' => $request->input('amount'),
                'applicant_email' => $request->input('applicant_email'),
                'applicant_name' => $request->input('applicant_name'),
                'applicant_phone' => $request->input('applicant_phone'),
                'bank_id' => $request->input('bank'),
                'comments' => $request->input('comments'),
                'contact_name' => $request->input('contact_name'),
                'contact_phone' => $request->input('contact_phone'),
                'delivery_date' => $request->input('delivery_date'),
                'delivery_type_id' => $request->input('delivery_type'),
                'inspection_date' => $request->input('inspection_date'),
                'inspection_time' => $request->input('inspection_time'),
                'inspector_id' => $request->input('inspector'),
                'is_paid' => $request->input('status_pago'),
                'payment_method_id' => $request->input('payment_method'),
                'register_type_id' => $request->input('register_type'),
                'updated_by_user_id' => $request->input('user_id')
            ];  
            
            $appraisal_request_id = $request->input('id');
            $user_id = $request->input('user_id');

            DB::transaction(function() use ($appraisal_request_id,$update_property_register,$update_appraisal_request_register)
            {
                DB::table('properties')
                ->where('appraisal_request_id',$appraisal_request_id )
                ->update($update_property_register);
            
                DB::table('appraisal_requests')
                ->where('id',$appraisal_request_id)
                ->update($update_appraisal_request_register);
            }, 5);
            
   
        }
        
        if ($request->input('spyForm') == 2) {

            $property = Property::find($request->property_id);
            $property->general_data = json_encode($request->inspeccion);

            $property->save();

            $levelModel = Level::where(['property_id' => $request->property_id])->get();


            $inspeccion_id = $request->property_id;
            $niveles = $request->niveles;
            $selections =  $request->coating_selected;

            //if count($levelModel) is true then only update is neccesary
            if (count($levelModel)) {

                $cantidadNivelesPrevios = count($levelModel);
                $cantidadNivelesNuevos = count($niveles);
    
                 /**It should be considered that the levels can be reduced in second updates */
                if ($cantidadNivelesPrevios <= $cantidadNivelesNuevos) {
                    /** if are same levels or more */ 
                    $pos = 1;

                    
                    foreach ($niveles as $index => $itemNivel) {
                        /** $request->niveles contains the levels that are sent from the form, updated */
                        /** $request->niveles ($niveles) and  $request->coating_selected ($selections) are
                         * iterating at same time
                         */
                        
                        $itemNivel['revestimientos']= $this->updateJsonCoatingsBase($selections, $index);
                        $nivelRowData = [   'property_id' => $inspeccion_id,
                                            'level' => $pos
                                        ];
                        $nivelNewData = ['internal_data' => json_encode($itemNivel)];
                        Level::updateOrCreate($nivelRowData,$nivelNewData);
                        ++$pos;
                    }
    
    
                } else {
                    /** if are less levels */ 

                    //TODO: Use a Index inside foreach instead of $pos
    
                    $pos = 1;
                    $nivelesCollection = Level::where('property_id', $inspeccion_id)->select('property_id','level')->orderBy('level')->get();
                    
    
                    foreach ($nivelesCollection as $itemCollection) {
                        $whereCondition = ['property_id' => $inspeccion_id, 'level' => $pos];
                        $isFind = false;
                        if ($pos <= $cantidadNivelesNuevos) {
                            $nivelesFind = Level::where($whereCondition)->get();
                            $isFind = (count($nivelesFind)) ? true : false; 
                        }
    
                        if ($isFind) {
                            $niveles[$pos - 1]['revestimientos']= $this->updateJsonCoatingsBase($selections, $pos - 1);
                            $nivelNewData = ['internal_data' => json_encode($niveles[$pos - 1]), 'level' => $pos];
                            Level::where($whereCondition)->update($nivelNewData);
                        } else {
                            Level::where($whereCondition)->delete();
                        }
                        ++$pos;
                    }
             
                }
            }else{
            //else, if count($levelModel) is false, then the levels are new records 
            // and is neccesary a level instance new


            //TODO: Use a Index inside foreach instead of $pos
            $pos = 0;
            foreach ($request->niveles as $index => $arrNivel) {
                $niveles = New Level;
                $niveles->property_id = $inspeccion_id;
                $niveles->level = $pos + 1;
                $arrNivel['revestimientos']= $this->updateJsonCoatingsBase($selections, $index);
                $niveles->internal_data = json_encode($arrNivel);
                $niveles->save();
                ++$pos;
            }

            }

        }

        // Property Information Section
        if ($request->input('spyForm') == 3) {

            //dd($request);
                    

            $update_property_register = [
                'cadastral_number' => $request->input('catastro'),
                'registration_number' => $request->input('registration_number'),                
                'book_number' => $request->input('book_number'),  
                'folio_number' => $request->input('folio_number'),                              
                'expedition_date' => $request->input('expedition_date'),
                'owner' => $request->input('property_owner'),     
                'land_area' => (is_null($request->input('property_land_area'))) ? 0 : $request->input('property_land_area'),
                'improvement_area' => (is_null($request->input('property_improvement_area'))) ? 0 : $request->input('property_improvement_area'), 
                'sale_amount' => (is_null($request->input('sale_amount'))) ? 0 : $request->input('sale_amount'), 
                'sale_type_id' => $request->input('sale_type')
            ]; 

            $appraisal_request_id = $request->input('appraisal_id');

            DB::transaction(function() use ($appraisal_request_id,$update_property_register)
            {
                DB::table('properties')
                ->where('appraisal_request_id',$appraisal_request_id )
                ->update($update_property_register);

            }, 5);

        }
    }        

    /**
     * coatingMaterialsBase
     * * this function return all arrays per group of materials 
     */

    public function coatingMaterialsBase()
    {
        $coatings = DB::table('coating_materials')
        ->select(
            'coating_id',
            'name',
            'text_value')
        ->get();

        $building = array();
        $building_collection = $coatings->where('coating_id',1);

        $windows = array();
        $windows_collection = $coatings->where('coating_id',2);

        $ceilings = array();
        $ceilings_collection = $coatings->where('coating_id',3);

        $loggers = array();
        $loggers_collection = $coatings->where('coating_id',4);

        $sanitary = array();
        $sanitary_collection = $coatings->where('coating_id',5);

       
        foreach ($building_collection as $item){
            $building[] = (object)['text' => $item->text_value, 'value' => false, 'name' => $item->name];
        }
        foreach ($windows_collection as $item){
            $windows[] = (object)['text' => $item->text_value, 'value' => false, 'name' => $item->name];
        }
        foreach ($ceilings_collection as $item){
            $ceilings[] = (object)['text' => $item->text_value, 'value' => false, 'name' => $item->name];
        }
        foreach ($loggers_collection as $item){
            $loggers[] = (object)['text' => $item->text_value, 'value' => false, 'name' => $item->name];
        }
        foreach ($sanitary_collection as $item){
            $sanitary[] = (object)['text' => $item->text_value, 'value' => false, 'name' => $item->name];
        }

        return compact('building','windows','ceilings','loggers','sanitary');

    }


    /**jsonCoatingsBase
     * * return a coating base json format
     */
    public function jsonCoatingsBase(){

        $coatings_group = $this->coatingMaterialsBase();

        $coating_json_entry = [];

        $building = ['pisos','pared_cocina','topes_cocina','pared_sanitarios','pisos_sanitarios'];
        $windows = ['ventanas'];
        $ceilings = ['techos','techo_sanitario'];
        $loggers = ['puerta_principal','puertas_interiores','jambas_puertas','closets','walking_closets','gabinetes_cocina'];
        $sanitary = ['ducha_sanitario'];


        
        foreach ($building as $entry_level1) {
            $coating_json_entry[$entry_level1] = [];
            foreach ($coatings_group['building'] as $materials){
                $coating_json_entry[$entry_level1][$materials->name] = array('text' => $materials->text, 'value' => $materials->value);
            }
        }

        foreach ($windows as $entry_level1) {
            $coating_json_entry[$entry_level1] = [];
            foreach ($coatings_group['windows'] as $materials){
                $coating_json_entry[$entry_level1][$materials->name] = array('text' => $materials->text, 'value' => $materials->value);
            }
        }

        foreach ($ceilings as $entry_level1) {
            $coating_json_entry[$entry_level1] = [];
            foreach ($coatings_group['ceilings'] as $materials){
                $coating_json_entry[$entry_level1][$materials->name] = array('text' => $materials->text, 'value' => $materials->value);
            }
        }

        foreach ($loggers as $entry_level1) {
            $coating_json_entry[$entry_level1] = [];
            foreach ($coatings_group['loggers'] as $materials){
                $coating_json_entry[$entry_level1][$materials->name] = array('text' => $materials->text, 'value' => $materials->value);
            }
        }

        foreach ($sanitary as $entry_level1) {
            $coating_json_entry[$entry_level1] = [];
            foreach ($coatings_group['sanitary'] as $materials){
                $coating_json_entry[$entry_level1][$materials->name] = array('text' => $materials->text, 'value' => $materials->value);
            }
        }



        return $coating_json_entry;

    }

    /**updateJsonCoatingsBase
     * * this function return a coating base json format updated, from arrays of selections of coating
     * 
     * @param Array $selections Array with selected coatings 
     * @param Integer  $current_level position or data of property level inside array
     */
    public function updateJsonCoatingsBase($selections, $current_level){


        $coating_json_entry = $this->jsonCoatingsBase();
        $building = ['pisos','pared_cocina','topes_cocina','pared_sanitarios','pisos_sanitarios'];
        $windows = ['ventanas'];
        $ceilings = ['techos','techo_sanitario'];
        $loggers = ['puerta_principal','puertas_interiores','jambas_puertas','closets','walking_closets','gabinetes_cocina'];
        $sanitary = ['ducha_sanitario'];

        $categories = array_merge($building,$windows,$ceilings,$loggers,$sanitary);



        foreach ($categories as $category){

            foreach($selections[$current_level][$category] as &$coating){

                $coating_json_entry[$category][$coating]['value'] = true;
            }

        }

        return $coating_json_entry;

    }

    public function generate_coating_arrays($data_coating){
        
        //coating categories or groups
        $coating_arrays = [
        'pisos' => [],
        'pared_cocina' => [],
        'topes_cocina' => [],
        'pared_sanitarios' => [],
        'pisos_sanitarios' => [],
        'ventanas' => [],
        'techos' => [],
        'techo_sanitario' => [],
        'puerta_principal' => [],
        'puertas_interiores' => [],
        'jambas_puertas' => [],
        'closets' => [],
        'walking_closets' => [],
        'gabinetes_cocina' => [],
        'ducha_sanitario' => [],
        ];


        
        foreach($data_coating as $index_l0 => $level){
            $coating_arrays_tmp = $coating_arrays;
            foreach($coating_arrays_tmp as $index_l1 => $category){
                //levels
                foreach($level->revestimientos->$index_l1 as $index_l2 => $coating){
                    //categories
                    if ($level->revestimientos->$index_l1->$index_l2->value === true){
                        $coating_arrays_tmp[$index_l1][] = $index_l2;
                    }
                }
                $result[$index_l0] =  $coating_arrays_tmp;
            }
            

        }

        return $result;

    }

    protected function fetchInspectionQtyPerDate($inspection_date) {
        
        return AppraisalRequest::where('appraisal_status_id',1)->where('inspection_date',$inspection_date)->count();

    }

    protected function fetchInspectionQtyPerInspector($inspector, $inspection_date) {
        
        return AppraisalRequest::where('appraisal_status_id',1)->where('inspection_date',$inspection_date)->where('inspector_id',$inspector)->count();

    }    

    protected function fetchInspectionQtyPerDeliveryDate($delivery_date) {
        
        return AppraisalRequest::where('appraisal_status_id',1)->where('delivery_date',$delivery_date)->count();

    }
    
    protected function appraisalRequests(Request $request) {
        //Appraisal Requests
       
        $memberUserId = $request->input('memberUserId');

        $userRoles = $this->fetchUserRoles($memberUserId);

        $result = collect([]);   

        foreach ($userRoles as $userRol){
            $result = $result->merge($this->fetchRequests($userRol));
        }

        return $result->unique()->values()->all();
    }

    protected function appraisalComparables(Request $request){

        $qry = DB::table('appraisal_requests_view')
        ->where('register_type_id','=',2)                    
        ->select(
        'appraisal_requests_view.id',
        'appraisal_requests_view.owner_user_id',
        'register_type_id',
        'register_type_name',
        'applicant_name',
        'applicant_phone',
        'applicant_email',
        'contact_name',
        'contact_phone',
        'delivery_type_id',
        'delivery_type_name',
        'bank_id',
        'bank_name',
        'amount',
        'payment_method_id',
        'payment_method_name',
        'is_paid',
        'inspection_date',
        'inspection_time',
        'delivery_date',
        'inspector_id',
        'inspector_name',
        'appraisal_status_id',
        'appraisal_status_name',
        'address',
        'comments',
        'created_by_user_id',
        'updated_by_user_id',
        'appraisal_request_id',
        'property_id',
        'property_type_id',
        'property_type_name',
        'property_use_status_id',
        'property_use_status_name',
        'property_number',
        'property_levels',
        'construction_year',
        'geo_coordinates',
        'cadastral_number',
        'registration_number',
        'book_number',
        'folio_number',
        'expedition_date',
        'is_new',
        'owner',
        'land_area',
        'improvement_area',
        'sale_date',
        'sale_amount',
        'appraisal_amount',
        'offer_amount',
        'sale_type_id',                
        'residence',
        'address_number',
        'address_street',
        'sector_id',
        'sector_name',
        'city_id',
        'city_name',
        'province_id',
        'province_name',
        'municipality_id',
        'municipality_name',
        'country_id',
        'country_name')
        ->get();

        return $qry;        
    }

    protected function appraisalTemplates(Request $request){
        $qry = DB::table('appraisal_requests_view')
        ->where('register_type_id','=',3)                    
        ->select(
        'appraisal_requests_view.id',
        'appraisal_requests_view.owner_user_id',
        'register_type_id',
        'register_type_name',
        'applicant_name',
        'applicant_phone',
        'applicant_email',
        'contact_name',
        'contact_phone',
        'delivery_type_id',
        'delivery_type_name',
        'bank_id',
        'bank_name',
        'amount',
        'payment_method_id',
        'payment_method_name',
        'is_paid',
        'inspection_date',
        'inspection_time',
        'delivery_date',
        'inspector_id',
        'inspector_name',
        'appraisal_status_id',
        'appraisal_status_name',
        'address',
        'comments',
        'created_by_user_id',
        'updated_by_user_id',
        'appraisal_request_id',
        'property_id',
        'property_type_id',
        'property_type_name',
        'property_use_status_id',
        'property_use_status_name',
        'property_number',
        'property_levels',
        'construction_year',
        'geo_coordinates',
        'cadastral_number',
        'registration_number',
        'book_number',
        'folio_number',
        'expedition_date',
        'is_new',
        'owner',
        'land_area',
        'improvement_area',
        'sale_date',
        'sale_amount',
        'appraisal_amount',
        'offer_amount',
        'sale_type_id',                
        'residence',
        'address_number',
        'address_street',
        'sector_id',
        'sector_name',
        'city_id',
        'city_name',
        'province_id',
        'province_name',
        'municipality_id',
        'municipality_name',
        'country_id',
        'country_name')
        ->get();

        return $qry;        

    }

    protected function requestsStatus(Request $request){
        // Appraisal Status
        return $this->fetchRequestStatus();
    }

    protected function requestsTypes(Request $request){
        $response = RegisterType::all();
        return json_encode($response);
    }

    protected function requestsCities(Request $request){
        $response = City::all();
        return json_encode($response);
    }

    protected function requestsSectors(Request $request){
        $response = Sector::all();
        return json_encode($response);
    }


    protected function fetchUserRoles($userId){
        $qry =  DB::table('member_users_roles_view')
                    ->where('member_user_id','=',$userId)
                    ->select('owner_user_id','member_user_id','role')
                    ->orderBy('member_user_id','asc')->orderBy('role','asc')
                    ->get();

        return $qry;
                    
    }

    protected function fetchTemplatesInfo(Request $request){
        
        $propertyType = $request->input('propertyType');
        $propertyLevels = $request->input('propertyLevels');

        $qry = DB::table('template_info_view')
                   ->where('property_type_id','=', $propertyType)
                   ->where('levels','=',$propertyLevels)
                   ->select('template_id',
                            'property_id  AS id',
                            'residence AS name',
                            'comments',
                            'property_type_name AS property_type')
                   ->get();

        return json_encode($qry);
    }

    protected function applyTemplate(Request $request){
        /**
         * $SrcPropertyId => property_id source
         * $DstPropertyId  => property_id destination
         */

        $SrcPropertyId = $request->input('template_property_id');
        $DstPropertyId = $request->input('appraisal_property_id');

        $SrcProperty = Property::find($SrcPropertyId);
        $DstProperty = Property::find($DstPropertyId);
        $DstProperty->general_data = $SrcProperty->general_data;
        $DstProperty->save();        

        $dataTemplate = Level::where('property_id',$SrcPropertyId)->get();

        foreach($dataTemplate as $item){
            $level = new Level;
            $level->property_id = $DstPropertyId;
            $level->level = $item['level'];
            $level->internal_data = $item['internal_data'];
            $level->save();
        }
    }

    protected function saveArrival(Request $request){
        $appraisalPropertyId = $request->input('appraisal_property_id');
        $arrivalGeocoords = $request->input('arrival_geocoords');
        $arrivalAccuracy = $request->input('arrival_accuracy');
        $arrivalDate = $request->input('arrival_date');
        $arrivalTime = $request->input('arrival_time');
        $userId = $request->input('user_id');

        $newArrival = new InspectionArrival;
        $newArrival->user_id = $userId;
        $newArrival->accuracy = $arrivalAccuracy;
        $newArrival->arrival_date = $arrivalDate;
        $newArrival->arrival_time = $arrivalTime;
        $newArrival->property_id = $appraisalPropertyId;
        $newArrival->geo_coordinates = $arrivalGeocoords;

        $newArrival->save();

        $property = Property::find($appraisalPropertyId);
        $property->inspection_arrival_id = $newArrival->id;
        $property->save();

        
    }

    protected function fetchArrival(Request $request){
        $appraisalPropertyId = $request->input('appraisal_property_id');

        $property = Property::find($appraisalPropertyId);

        if (!is_null($property->inspection_arrival_id)){
            return InspectionArrival::find($property->inspection_arrival_id);
        } else {
            return false;
        }
        
        

    }


}


