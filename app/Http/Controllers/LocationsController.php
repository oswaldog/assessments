<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Province;
use App\Municipality;
use App\City;
use App\Sector;


class LocationsController extends Controller
{
    //
    function fetchProvinces(Request $request)
    {
        return Province::all();
    }

    function fetchMunicipalities(Request $request)
    {
        $id = $request->input('id');
        return Municipality::where('province_id',$id)->get();
    }

    function fetchCities(Request $request)
    {
        $id = $request->input('id');
        return City::where('municipality_id',$id)->get();
    }

    function fetchSectors(Request $request)
    {
        $id = $request->input('id');
        return Sector::where('city_id',$id)->get();
    }
}
