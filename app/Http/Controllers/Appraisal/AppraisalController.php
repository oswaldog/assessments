<?php

namespace App\Http\Controllers\Appraisal;

use App\Http\Controllers\Controller;
use App\AppraisalRequest;
use App\AppraisalStatus;
use Illuminate\Http\Request;

class AppraisalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function updateAppraisalStatus(Request $request)
    {

        // ['id' => 1, 'name' => 'Agendada', 'description' => 'Solicitud asignada en espera de ser inspeccionada'],
        // ['id' => 2, 'name' => 'Inspeccionada', 'description' => 'Solicitud inspeccionada en espera de ser revisada'],
        // ['id' => 3, 'name' => 'Revisada', 'description' => 'Solicitud revisada en espera de ser evaluada'],
        // ['id' => 4, 'name' => 'Evaluada', 'description' => 'Solicitud evaluada en espera de ser aprobada'],
        // ['id' => 5, 'name' => 'Aprobada', 'description' => 'Solicitud aprobada. Proceso completado'],
        // ['id' => 6, 'name' => 'Cerrada', 'description' => 'Solicitud no completada y cerrada'],


        //
        $is_appraisal_close = $request->params['appraisal_close'];
        $appraisal_request_id = $request->params['appraisal_request_id'];
        $appraisal_status_id = $request->params['appraisal_status_id'];
        
        $foundAppraisalRecord = 0;
        $statusAppraisalDescription = '';
        $statusAppraisalName = '';

        if ($appraisal_status_id >= 1 && $appraisal_status_id <= 4){
            $appraisalRequest = AppraisalRequest::where('id',$appraisal_request_id)
                                ->where('appraisal_status_id',$appraisal_status_id)
                                ->first();
            if($appraisalRequest){
                if ($is_appraisal_close) {
                    $appraisal_status_id = 6;
                } else {
                    ++$appraisal_status_id;
                }
                $appraisalRequest->appraisal_status_id = $appraisal_status_id;
                $appraisalRequest->save();
                $foundAppraisalRecord = 1;
            }
        }

        if ($foundAppraisalRecord === 1){
            $appraisalStatus = AppraisalStatus::find($appraisal_status_id);
            if ($appraisalStatus) {
                $statusAppraisalDescription = $appraisalStatus->description;
                $statusAppraisalName = $appraisalStatus->name;
            }

        }

        return compact(['foundAppraisalRecord','statusAppraisalDescription','statusAppraisalName']);
    }


}
