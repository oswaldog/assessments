<?php

namespace App\Http\Controllers\Comparables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AppraisalRequest;
use App\Property;
use App\Attachment;
use App\Level;
use DB;

class ComparablesController extends Controller
{
    //
    public function fetchRequestData(Request $request)
    {
        $request_id = $request->input('appraisalRequestId');
        $propertyInfo = DB::table('comparables_view')
                        ->where('appraisal_request_id','=',$request_id)
                        ->select(
                            "property_id",
                            "appraisal_request_id",
                            "property_type_id",
                            "property_type_name",
                            "property_use_status_id",
                            "property_use_status_name",
                            "property_number",
                            "property_levels",
                            "construction_year",
                            "geo_coordinates",
                            "cadastral_number",
                            "registration_number",
                            "book_number",
                            "folio_number",
                            "expedition_date",
                            "is_new",
                            "owner",
                            "land_area",
                            "improvement_area",
                            "sale_date",
                            "sale_amount",
                            "appraisal_amount",
                            "offer_amount",
                            "sale_type_id",
                            "total_rooms",
                            "total_baths",
                            "total_parking_spots",
                            "residence",
                            "address_number",
                            "address_street",
                            "sector_id",
                            "sector_name",
                            "city_id",
                            "city_name",
                            "province_id",
                            "province_name",
                            "municipality_id",
                            "municipality_name",
                            "country_id",
                            "country_name",
                            "sauna",
                            "gazebo",
                            "jacuzzi",
                            "picuzzi",
                            "piscina",
                            "gimnasio_amenidades",
                            "campo_golf",
                            "area_juegos",
                            "canchas_tenis",
                            "sala_choferes",
                            "salon_reuniones",
                            "terraza_techada",
                            "canchas_baloncesto",
                            "terraza_destechada",
                            "photo_path",
                            "photo_relative_path",
                            "photo_name",
                            "func_appraisal_areas_json")
                        ->get();

        return $propertyInfo;
    }

    public function fetchComparables(Request $request)
    {
        $form = json_decode($request->input('formData'));

        $arrSector = array();
        $provinceId = null;
        $municipalityId = null;
        $cityId = null;

        $qry = DB::table('comparables_view')
                    ->join('appraisal_requests', function($join){
                        $join
                            ->on('comparables_view.appraisal_request_id','=','appraisal_requests.id')
                            ->where('appraisal_requests.register_type_id','=','2')
                            ->orWhere(function($query){
                                $query
                                    ->where('appraisal_requests.register_type_id','=','1')
                                    ->where('appraisal_requests.appraisal_status_id','=','5');

                            });

                    })
                    ->where('property_type_id','=',$form->property_type);

        if (!empty($form->sectors))
        {
            foreach($form->sectors as $sector)
            {
                $arrSector[] = $sector->id;
            }

            $qry = $qry->whereIn('sector_id',$arrSector);
        }
       
        if (isset($form->locationSelect->province->id))
        {
            $provinceId = $form->locationSelect->province->id;
            $qry = $qry->where('province_id',$provinceId);
        }

        if (isset($form->locationSelect->municipality->id))
        {
            $municipalityId = $form->locationSelect->municipality->id;
            $qry = $qry->where('municipality_id',$municipalityId);
        }

        if (isset($form->locationSelect->city->id))
        {
            $cityId = $form->locationSelect->city->id;
            $qry = $qry->where('city_id',$cityId);
        }

        if (!empty($form->quantity->hab))
        {
            $hab = $form->quantity->hab;
            if (is_int($hab)){
                $qry = $qry->where('total_rooms',$hab);
            } else {
                $hab = 9;
                $qry = $qry->where('total_rooms','>=',$hab);
            }
        }

        if (!empty($form->quantity->bath))
        {
            $bath = $form->quantity->bath;
            if (is_int($bath)){
                $qry = $qry->where('total_baths',$bath);
            } else {
                $bath = 9;
                $qry = $qry->where('total_baths', '>=',$bath);                
            }
        }
        
        if (!empty($form->quantity->parking))
        {
            $parking = $form->quantity->parking;
            if (is_int($parking)){
                $qry = $qry->where('total_parking_spots',$parking);
            } else {
                $parking = 9;
                $qry = $qry->where('total_parking_spots', '>=',$parking);                
            }            
        }
        
        if (!empty($form->quantity->levels))
        {
            $levels = $form->quantity->levels;
            if (is_int($levels)){
                $qry = $qry->where('property_levels',$levels);
            } else {
                $levels = 9;
                $qry = $qry->where('property_levels', '>=',$levels);                
            }            
        }

        if ($form->amenidades->piscina !== 0) {
            $has_piscina = ($form->amenidades->piscina == 1) ? 'SI' : 'NO';
            $qry = $qry->where('piscina',$has_piscina);
        }
        if ($form->amenidades->picuzzi !== 0) {
            $has_picuzzi = ($form->amenidades->picuzzi == 1) ? 'SI' : 'NO';
            $qry = $qry->where('picuzzi',$has_picuzzi);
        }
        if ($form->amenidades->jacuzzi !== 0) {
            $has_jacuzzi = ($form->amenidades->jacuzzi == 1) ? 'SI' : 'NO';
            $qry = $qry->where('jacuzzi',$has_jacuzzi);
        }
        if ($form->amenidades->salon_reuniones !== 0) {
            $has_salon_reuniones = ($form->amenidades->salon_reuniones == 1) ? 'SI' : 'NO';
            $qry = $qry->where('salon_reuniones',$has_salon_reuniones);
        }
        if ($form->amenidades->gazebo !== 0) {
            $has_gazebo = ($form->amenidades->gazebo == 1) ? 'SI' : 'NO';
            $qry = $qry->where('gazebo',$has_gazebo);
        }
        if ($form->amenidades->area_juegos !== 0) {
            $has_area_juegos = ($form->amenidades->area_juegos == 1) ? 'SI' : 'NO';
            $qry = $qry->where('area_juegos',$has_area_juegos);
        }
        if ($form->amenidades->gimnasio !== 0) {
            $has_gimnasio = ($form->amenidades->gimnasio == 1) ? 'SI' : 'NO';
            $qry = $qry->where('gimnasio_amenidades',$has_gimnasio);
        }
        if ($form->amenidades->terraza_techada !== 0) {
            $has_terraza_techada = ($form->amenidades->terraza_techada == 1) ? 'SI' : 'NO';
            $qry = $qry->where('terraza_techada',$has_terraza_techada);
        }
        if ($form->amenidades->terraza_destechada !== 0) {
            $has_terraza_destechada = ($form->amenidades->terraza_destechada == 1) ? 'SI' : 'NO';
            $qry = $qry->where('terraza_destechada',$has_terraza_destechada);
        }
        if ($form->amenidades->sauna !== 0) {
            $has_sauna = ($form->amenidades->sauna == 1) ? 'SI' : 'NO';
            $qry = $qry->where('sauna',$has_sauna);
        }
        if ($form->amenidades->campo_golf !== 0) {
            $has_campo_golf = ($form->amenidades->campo_golf == 1) ? 'SI' : 'NO';
            $qry = $qry->where('campo_golf',$has_campo_golf);
        }
        if ($form->amenidades->canchas_baloncesto !== 0) {
            $has_canchas_baloncesto = ($form->amenidades->canchas_baloncesto == 1) ? 'SI' : 'NO';
            $qry = $qry->where('canchas_baloncesto',$has_canchas_baloncesto);
        }
        if ($form->amenidades->canchas_tenis !== 0) {
            $has_canchas_tenis = ($form->amenidades->canchas_tenis == 1) ? 'SI' : 'NO';
            $qry = $qry->where('canchas_tenis',$has_canchas_tenis);
        }
        if ($form->amenidades->sala_choferes !== 0) {
            $has_sala_choferes = ($form->amenidades->sala_choferes == 1) ? 'SI' : 'NO';
            $qry = $qry->where('sala_choferes',$has_sala_choferes);
        }


        $qry = $qry->select(
            "property_id",
            "appraisal_request_id",
            "property_type_id",
            "property_type_name",
            "property_use_status_id",
            "property_use_status_name",
            "property_number",
            "property_levels",
            "construction_year",
            "geo_coordinates",
            "cadastral_number",
            "registration_number",
            "book_number",
            "folio_number",
            "expedition_date",
            "is_new",
            "owner",
            "land_area",
            "improvement_area",
            "sale_date",
            "sale_amount",
            "appraisal_amount",
            "offer_amount",
            "sale_type_id",
            "total_rooms",
            "total_baths",
            "total_parking_spots",
            "residence",
            "address_number",
            "address_street",
            "sector_id",
            "sector_name",
            "city_id",
            "city_name",
            "province_id",
            "province_name",
            "municipality_id",
            "municipality_name",
            "country_id",
            "country_name",
            "sauna",
            "gazebo",
            "jacuzzi",
            "picuzzi",
            "piscina",
            "gimnasio_amenidades",
            "campo_golf",
            "area_juegos",
            "canchas_tenis",
            "sala_choferes",
            "salon_reuniones",
            "terraza_techada",
            "canchas_baloncesto",
            "terraza_destechada",
            "photo_path",
            "photo_relative_path",
            "photo_name",
            "func_appraisal_areas_json")
        ->get();

       return $qry;

    }


    public function fetchAnalysisData(Request $request)
    {
        $elements = $request->input('items');

        $qry = DB::table('comparables_view')
            ->whereIn('appraisal_request_id',$elements)
            ->select(
            "property_id",
            "appraisal_request_id",
            "property_type_id",
            "property_type_name",
            "property_use_status_id",
            "property_use_status_name",
            "property_number",
            "property_levels",
            "construction_year",
            "geo_coordinates",
            "cadastral_number",
            "registration_number",
            "book_number",
            "folio_number",
            "expedition_date",
            "is_new",
            "owner",
            "land_area",
            "improvement_area",
            "sale_date",
            "sale_amount",
            "appraisal_amount",
            "offer_amount",
            "sale_type_id",
            "total_rooms",
            "total_baths",
            "total_parking_spots",
            "residence",
            "address_number",
            "address_street",
            "sector_id",
            "sector_name",
            "city_id",
            "city_name",
            "province_id",
            "province_name",
            "municipality_id",
            "municipality_name",
            "country_id",
            "country_name",
            "sauna",
            "gazebo",
            "jacuzzi",
            "picuzzi",
            "piscina",
            "gimnasio_amenidades",
            "campo_golf",
            "area_juegos",
            "canchas_tenis",
            "sala_choferes",
            "salon_reuniones",
            "terraza_techada",
            "canchas_baloncesto",
            "terraza_destechada",
            "photo_path",
            "photo_relative_path",
            "photo_name",
            "func_appraisal_areas_json")
        ->get();

       return $qry;

    }

}
