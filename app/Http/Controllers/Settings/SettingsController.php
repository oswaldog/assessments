<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\User;
use App\Province;
use App\WorkTeam;
use DB;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Update the user's profile information.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function updateProfile(Request $request)
    {   
        $user = $request->user();

        $request->user()->update([
            'password' => bcrypt($request->password),
        ]);

        return tap($user)->update($request->only('name', 'email','codia_code','itado_code','phone','province_id'));

    }

    protected function index(Request $request){
        $id = $request->input('id');        
        switch($id){
            case 1:
                return $this->fetchData();
            case 2:
                $name           = $request->input('name');
                $phone          = $request->input('phone');
                $province       = $request->input('province_id');
                $owner_user_id  = $request->input('owner_user_id');
                return $this->lookUpUsers($name,$phone,$province,$owner_user_id);
            case 3:
                $owner_user_id  = $request->input('owner_user_id');
                return $this->lookUpUsersMembers($owner_user_id);
        }
   }

    protected function fetchData(){
        //Regions (Provinces)
        $qry = Province::all();
        return $qry;
    }
    /**
     * Look up Users
     * @param name, phone and province
     * @return  response
     */
    protected function lookUpUsers($name,$phone,$province,$owner_user_id){

        $where = [];
        if ($province !== '0') {
            array_push($where,['users.province_id','=', $province]);
        } else {
            array_push($where,['users.province_id','>', $province]);
        }

        if ($phone) {
            array_push($where,['users.phone', 'like', '%' . $phone . '%']);
        }

        if ($name) {
            array_push($where,['users.name', 'ilike', '%' . $name . '%']);
        }

        return  DB::table('users')
                    ->whereNotIn('users.id', function($query) use ($owner_user_id)
                    {
                        $query->select('member_user_id')
                            ->from('work_teams')
                            ->whereRaw('work_teams.owner_user_id = '.$owner_user_id);
                    })
                    ->join('provinces', 'users.province_id', '=', 'provinces.id')
                    ->leftjoin('administratives_roles_view', 'users.id', '=', 'administratives_roles_view.member_user_id')
                    ->where($where)
                    ->select('users.id','users.name','users.phone','provinces.name AS province','administratives_roles_view.is_administrative')
                    ->get();

    }

    protected function lookUpUsersMembers($owner_user_id){

        return DB::select('SELECT active_permissions_view.id AS permissions_id, users.id,users.name,users.phone,provinces.name AS province,administratives_roles_view.is_administrative, active_permissions_view.array_values
                            FROM users
                            JOIN provinces 
                                ON users.province_id = provinces.id
                            JOIN active_permissions_view
                                ON users.id = active_permissions_view.member_user_id AND 
                                active_permissions_view.owner_user_id = :owner_user_id
                            LEFT JOIN administratives_roles_view
                                ON users.id = administratives_roles_view.member_user_id AND 
                                administratives_roles_view.owner_user_id <> :owner_user_id'
                            , ['owner_user_id' => $owner_user_id]);                
    }

    protected function addTeamMembers(Request $request){

        /**
         * TODO: add stored procedures to handle roles register and avoid contreign with admistratives role rule
         */

        $work_team = New WorkTeam;

        $work_team->owner_user_id = $request->owner_user_id;
        $work_team->member_user_id = $request->member_user_id;
        $work_team->permissions = $this->roles_json($request->permission);

        $work_team->save();

    }

    protected function updateTeamMembers(Request $request)
    {
        $work_team = WorkTeam::where('owner_user_id',$request->owner_user_id)
                        ->where('member_user_id',$request->member_user_id)
                        ->first();
        $work_team->permissions = $this->roles_json($request->permission);
        $work_team->save();
       
    }

    protected function deleteTeamMembers($id)  
    {   
        $work_team = WorkTeam::where('id', $id)->firstorfail()->delete(); 
    }

    protected function roles_json($selected){
        /**
         * ! text: role name, 
         * ! value: position value on input checkbox
         * ! status: if is true then user has that role 
         */
        $roles = '[
            { "text": "Administrativo", "value": "0", "status": false},
            { "text": "Inspector",      "value": "1", "status": false },
            { "text": "Evaluador",      "value": "2", "status": false },
            { "text": "Aprobador",      "value": "3", "status": false }
            ]';

        $jsonRoles = json_decode($roles,true);

        foreach($selected as $key=>$value)
        {
            $jsonRoles[$value]['status'] = true;
        }
        //return json_encode($jsonRoles);
        return $jsonRoles;
         
    }

    protected function hasUserRole($member_user_id){
        /**
         * returns how many roles the user has
         */
        $hasRole = WorkTeam::where('member_user_id', $member_user_id)->count();
        return $hasRole;
    }


    protected function getRolesUser($member_user_id){
        $roles = array("administrative" => false, "inspector" => false, "evaluator" => false, "approver" => false);
        $memberships = WorkTeam::where('member_user_id', $member_user_id)->get();

        /**
         * returns the types of roles that the user has
         * 
         * 0 => administrative
         * 1 => inspector
         * 2 => evaluator
         * 3 => approver
         */

        if (!$memberships->isEmpty()){
            foreach ($memberships as $membership){
                if ($roles['administrative'] === false){
                    if($membership->permissions[0]['status'] === true){
                        $roles['administrative'] = true;
                    }
                }
                if ($roles['inspector'] === false){
                    if($membership->permissions[1]['status'] === true){
                        $roles['inspector'] = true;
                    }
                }
                if ($roles['evaluator'] === false){
                    if($membership->permissions[2]['status'] === true){
                        $roles['evaluator'] = true;
                    }
                }
                if ($roles['approver'] === false){
                    if($membership->permissions[3]['status'] === true){
                        $roles['approver'] = true;
                    }
                }
            }
        }
        
        return  $roles;

    }

    protected function getAdminOwnerId($member_user_id){
        /**
         * Returns the owner_user_id where user has admin role, otherwise returns null, 
         * 
         * 0 => administrative
         */
        $admin = WorkTeam::where('member_user_id',$member_user_id)->where('permissions->0->status','true')->first();

        return ($admin) ? $admin->owner_user_id : null;

    }


    protected function hasInspectors($owner_user_id){
        /**
         * returns quantity of  WorkTeam Inspectors
         */
        
        

        $hasInspectors = WorkTeam::where('owner_user_id',$owner_user_id)->where('permissions->1->status','true')->count();

        return $hasInspectors;

    }



    protected function workTeamInfo(Request $request){

        $memberUserId = $request->input('memberUserId');

        $hasUserRole = $this->hasUserRole($memberUserId);

        $userRoles = $this->getRolesUser($memberUserId);

        $adminOwnerId = $this->getAdminOwnerId($memberUserId);

        $hasInspectors = $this->hasInspectors($adminOwnerId);

        return compact(['hasUserRole','userRoles','adminOwnerId','hasInspectors']); 
    }

}

