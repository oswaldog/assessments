<?php

namespace App\Http\Controllers\Attachment;

use App\Attachment;
use App\AppraisalRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DateTime;
use DB;
use ImageOptimizer;
use Spatie\Image\Image;

class AttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->input('id');        
        switch($id){

            case 1: 
                $appraisal_request_id = $request->input('appraisal_request_id');
                $type_file = $request->input('type_file');
                $files = $this->fetchAttachments($appraisal_request_id, $type_file);
                $url = url('/');
                return compact(['files','url']);
            case 2: 
                $appraisal_request_id = $request->input('appraisal_request_id');
                $result = $this->checkIfHasFilesStored($appraisal_request_id);
                return $result;
        }   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addAttachments(Request $request)
    {
        //dd($request->files);
        //
        if (count($request->files)) {

            $appraisal_request_id = $request->input('appraisal_request_id');
            $file_type = $request->input('file_type');
            $user_id = $request->input('user_id');

            $today = new \DateTime('NOW');
            
            foreach ($request->file('attachements') as  $files) {
              //  if ($files->isValid()) {
                    $prefix = $today->format('YmdHis');
                    $name = preg_replace('/\s+/', '_', $prefix.'_'.$files[0]->getClientOriginalName());
                    $real_name = preg_replace('/\s+/', '_', $files[0]->getClientOriginalName());
                    $path = public_path() . '/uploads/' . $appraisal_request_id . '/'.$file_type;
                    $relative_path = '/uploads/' . $appraisal_request_id . '/'.$file_type;
                    $type = $files[0]->getClientOriginalExtension();
                    $size = $files[0]->getSize();

                    // store in filesystem
                    $files[0]->move($path, $name);

                    if ($file_type === 'img') {

                        //Optimize
                        ImageOptimizer::optimize($path.'/'.$name);

                        $path_thumb = public_path() . '/uploads/' . $appraisal_request_id . '/' . 'thumb';

                        if (!file_exists($path_thumb)) {
                            mkdir($path_thumb, 0777, true);
                        }

                        Image::load($path.'/'.$name)
                        ->width(266)
                        ->height(355)
                        ->save($path_thumb.'/'.$name);

                    }

                    // store in db
                    $attachmentModel = new Attachment();
                    $attachmentModel->appraisal_request_id = $appraisal_request_id;
                    $attachmentModel->user_id = $user_id;
                    $attachmentModel->path = $path;
                    $attachmentModel->relative_path = $relative_path;
                    $attachmentModel->name = $name;
                    $attachmentModel->real_name = $real_name;
                    $attachmentModel->type = $type;
                    $attachmentModel->size = $size;
                    //$attachmentModel->save();   
                    AppraisalRequest::find($appraisal_request_id)->attachments()->save($attachmentModel);  
             //   }           
            }

            if ($file_type === 'img') {
                $this->reorderPhotosByRealName($appraisal_request_id);
            }

        }
    }

    /**
     * Function to sort every time photos are uploaded by the real name of the photos  
     */

    protected function reorderPhotosByRealName($appraisal_request_id){

        $qry = DB::table('attachments_view')
        ->select(
            'id',
            'file_type',
            'real_name',
             DB::raw('SUBSTRING(real_name,0,LENGTH(real_name)-3) AS tmp_real_name'),
             DB::raw("NULLIF(regexp_replace(real_name, '\D', '', 'g'),'')::INTEGER AS num_part")
            )
        ->where('appraisal_request_id','=',$appraisal_request_id)
        ->where('file_type','=','img')
        ->orderBy('num_part')
        ->orderBy('tmp_real_name')
        ->get();   

        foreach ($qry as $key => $item)
        { 
            $file = Attachment::find($item->id);
            $file->pos = $key+1;            
            $file->save();
        }

    }

    /**
     *  Function to delete docs type files.
     */
    protected function deleteAttachment($id){ 
        
        $attachment = Attachment::where('id',$id)->first();

        $file = $attachment->path.'/'.$attachment->name;

        if (explode('/', $attachment->relative_path, 4)[3] === 'img') {
            
            $thumb_path = str_replace("img","thumb",$attachment->path);
            $thumb = $thumb_path.'/'.$attachment->name;

            if(file_exists($thumb)){
                unlink($thumb);
            }
            
        }

        if(file_exists($file)){
            unlink($file);
        }

        $attachment->delete();
        
    }

    protected function fetchAttachments($appraisal_request_id, $type_file){
        $qry = DB::table('attachments_view')
        ->select(
            'id',
            'appraisal_request_id',
            'user_id',
            'file_type',
            'path',
            'relative_path',
            'name',
            'real_name',
            'file_format',
            'size',
            'pos'
            )
        ->where('appraisal_request_id','=',$appraisal_request_id)
        ->where('file_type','=',$type_file)
        ->orderBy('pos')
        ->orderBy('id')
        ->get();

        return $qry;

    }


    protected function checkIfHasFilesStored($appraisal_request_id){
        $qry = DB::table('total_attachments_view')
        ->select(
            'appraisal_request_id',
            'total_img',
            'total_doc'
            )
        ->where('appraisal_request_id','=',$appraisal_request_id)
        ->get();

        return $qry;

    }    

    /**
     * Function to change position of a photo
     */
    protected function updatePosition(Request $request){

        $appraisal_request_id = $request->input('appraisal_request_id');
        $array_data = $request->input('array_data');

        foreach ($array_data as $item){
            $file = Attachment::find($item['id']);
            $file->pos = $item['pos'];            
            $file->save();
        }
    }

    protected function deleteOneOrMoreImages(Request $request){

        $arr_ima = $request->input('arr_ima');
        
        foreach ($arr_ima as $key => $value) {

            $this->deleteAttachment($value);

        }
        
    }

    /**
     * function to download one or more photos selected appraisal request
     */
    protected function downloadOneOrMoreImages(Request $request
    ){
        $arr_ima = $request->input('arr_ima');
        $file_name = $request->input('file_name'); 
        $appraisal_request_id = $request->input('appraisal_request_id');
        $path = public_path() . '/uploads/' . $appraisal_request_id . '/';

        if (file_exists($path.$file_name)){
            /** a previus file exists */
            unlink($path.$file_name);
        }

        $attachments = Attachment::whereIn('id',$arr_ima)->get();
        $zip = new \ZipArchive;
        if ($zip->open($path.$file_name, \ZipArchive::CREATE) === TRUE) { 
            
            
            foreach ($attachments as $attachment){

                if (explode('/', $attachment->relative_path, 4)[3] === 'img') {
    
                    $zip->addFile($attachment->path.'/'.$attachment->name,$attachment->real_name); 
    
                }
    
            }

 
            $zip->close();
        }
         $headers = array(
                'Content-Type' => 'application/octet-stream',
            );
        $filetopath=$path.'/'.$file_name;
        if(file_exists($filetopath)){
            return response()->download($filetopath,$file_name,$headers);

        }
        return ['status'=>'file does not exist'];
   
        
    }

    /**
     * Function to clean and delete all photos per appraisal request id
     */
    protected function deleteMultiImages(Request $request){

        $appraisal_request_id = $request->input('appraisal_request_id');

        $attachments = Attachment::where('appraisal_request_id',$appraisal_request_id)->get();

        foreach ($attachments as $attachment){

            if (explode('/', $attachment->relative_path, 4)[3] === 'img') {
                $file = $attachment->path.'/'.$attachment->name;
                $thumb_path = str_replace("img","thumb",$attachment->path);
                $thumb = $thumb_path.'/'.$attachment->name;

                if(file_exists($file)){
                    unlink($file);
                }

                if(file_exists($thumb)){
                    unlink($thumb);
                }
                
                $attachment->delete();

            }

        }

    }

    /**
     * Function to download all images per appraisal request id
     */
    protected function downloadMultiImages(Request $request){
        $appraisal_request_id = $request->input('appraisal_request_id');
        
        $file_name = 'photos.zip';
        $path = public_path() . '/uploads/' . $appraisal_request_id . '/';

        if (file_exists($path.$file_name)){
            /** a previus file exists */
            unlink($path.$file_name);
        }

        $attachments = Attachment::where('appraisal_request_id',$appraisal_request_id)->get();
        $zip = new \ZipArchive;
        if ($zip->open($path.$file_name, \ZipArchive::CREATE) === TRUE) { 
            
            
            foreach ($attachments as $attachment){

                if (explode('/', $attachment->relative_path, 4)[3] === 'img') {
    
                    $zip->addFile($attachment->path.'/'.$attachment->name,$attachment->real_name); 
    
                }
    
            }

 
            $zip->close();
        }
         $headers = array(
                'Content-Type' => 'application/octet-stream',
            );
        $filetopath=$path.'/'.$file_name;
        if(file_exists($filetopath)){
            return response()->download($filetopath,$file_name,$headers);

        }
        return ['status'=>'file does not exist'];
    }

    /** 
     * Function created to comparables' photos selection to be used on cost analysis
     */

    public function photosComparables(Request $request)
    {
        
        $appraisal_request_id = $request->input('appraisalRequestId');
        $qry = Attachment::where('appraisal_request_id',$appraisal_request_id)
                ->whereIn('type',['png','jpg'])
                ->select(['id','appraisal_request_id','name','relative_path'])
                ->get();
        return $qry;

    }

}
