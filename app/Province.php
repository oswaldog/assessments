<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model 
{

    protected $table = 'provinces';
    public $timestamps = false;
    protected $fillable = ['name','country_id'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function municipalities()
    {
        return $this->hasMany('App\Municipality');
    }


}