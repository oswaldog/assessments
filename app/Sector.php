<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model 
{

    protected $table = 'sectors';
    public $timestamps = false;
    protected $fillable = ['name','city_id'];

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function requests()
    {
        return $this->hasMany('App\Request');
    }


}