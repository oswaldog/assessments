<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkTeam extends Model
{
    //
    protected $table = 'work_teams';


    //To handle json field as an array
    protected $casts = [
        'permissions' => 'array',
    ];
}
