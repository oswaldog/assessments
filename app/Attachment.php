<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    //
    protected $fillable = ['appraisal_request_id','user_id','path','name','type','size'];


    public function appraisal_request()
    {
        return $this->belongsTo('App\AppraisalRequest');
    }

}
