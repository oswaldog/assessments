<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property_Use_Status extends Model 
{

    protected $table = 'property_use_status';
    public $timestamps = false;

}