<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('soporte@rdappraisal.com', 'RD Appraisal')
            ->subject('Restablecer Contraseña')
            ->greeting('Hola!')
            ->line('Usted esta recibiendo este correo electrónico porque fue generada una solicitud de restablecimiento de contraseña para su cuenta.')
            ->action('Restablecer la Contraseña', url(config('app.url').'/password/reset/'.$this->token).'?email='.urlencode($notifiable->email))
            ->line('Si no solicitó un restablecimiento de contraseña, haga caso omiso a esta notificación y no reproduzca ninguna otra acción.')
            ->salutation('Saludos cordiales');
    }
}
