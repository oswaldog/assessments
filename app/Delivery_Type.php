<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_Type extends Model 
{

    protected $table = 'delivery_types';
    public $timestamps = false;

}