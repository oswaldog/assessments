<?php

namespace App\Exports;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
// use Maatwebsite\Excel\Concerns\WithMapping;


class InspectionsAmenitiesExport implements 
    FromCollection, 
    ShouldAutoSize, 
    // WithMapping, 
    WithHeadings,
    WithStyles,
    WithTitle

{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $inspections = DB::table('inspections_view')
                        ->select(
                            'piscina',
                            'picuzzi',
                            'jacuzzi',
                            'salon_reuniones',
                            'gazebo',
                            'area_juegos',
                            'gimnasio_amenidades',
                            'terraza_techada',
                            'terraza_destechada',
                            'sauna',
                            'campo_golf',
                            'canchas_baloncesto',
                            'canchas_tenis',
                            'sala_choferes'
                            )
                        ->where('appraisal_request_id','=',$this->id)
                        ->get();


        return $inspections;

    }

    // public function map($item): array
    // {
    //     return [
    //         $item->id,
    //         $item->applicant_name,
    //         $item->applicant_email
    //     ];
    // }


    public function headings(): array
    {
        return [
            'Piscina',
            'Picuzzi',
            'Jacuzzi',
            'Salón de reuniones',
            'Gazebo',
            'Área de juegos para niños',
            'Gimnasio',
            'Terraza techada',
            'Terraza destechada',
            'Sauna',
            'Campo de golf',
            'Canchas de baloncesto',
            'Canchas de tenis',
            'Sala de espera para choferes '
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Amenidades';
    }
}
