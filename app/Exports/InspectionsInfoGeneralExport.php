<?php

namespace App\Exports;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
// use Maatwebsite\Excel\Concerns\WithMapping;

class InspectionsInfoGeneralExport implements 
    FromCollection, 
    ShouldAutoSize, 
    // WithMapping, 
    WithHeadings,
    WithStyles,
    WithTitle
{

    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $inspections = DB::table('inspections_view')
                        ->select(
                            'entrada_lobby',
                            'marquesina',
                            'galeria',
                            'planta_full',
                            'areas_comunes',
                            'porton_electrico',
                            'gas_tubería',
                            'intercom',
                            'pozo',
                            'cisterna',
                            'sistema_incendio',
                            'camaras_seguridad',
                            'verja_perimetral',
                            'herraje_puerta_principal',
                            'herraje_ventanas',
                            'aa_central',
                            'niveles_estructura',
                            'ascensor',
                            'ascensor_carga',
                            'shutters',
                            'parqueos_aire_libre',
                            'parqueos_techados',
                            'condiciones_generales'
                            )
                        ->where('appraisal_request_id','=',$this->id)
                        ->get();


        return $inspections;

    }

    // public function map($item): array
    // {
    //     return [
    //         $item->id,
    //         $item->applicant_name,
    //         $item->applicant_email
    //     ];
    // }


    public function headings(): array
    {
        return [
                'Entrada / Lobby',
                'Marquesina',
                'Galeria',
                'Planta Full',
                'Planta Areas Comunes',
                'Portón Eléctrico',
                'Gas por Tubería',
                'Intercom',
                'Pozo',
                'Cisterna',
                'Sistema Contra Incendios',
                'Cámaras de Seguridad',
                'Verja Perimetral',
                'Herraje en Puerta Principal',
                'Herraje en Ventanas',
                'A/A Central ',
                'Niveles de la Estructura',
                'Ascensor',
                'Ascensor de Carga',
                'Shutters',
                'Parqueos Aire Libre',
                'Parqueos Techados',
                'Condiciones Generales'
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Info General';
    }
    

}
