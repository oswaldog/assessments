<?php

namespace App\Exports;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
// use Maatwebsite\Excel\Concerns\WithMapping;


class InspectionsAreaExport implements 
    FromCollection, 
    ShouldAutoSize, 
    // WithMapping, 
    WithHeadings,
    WithStyles,
    WithTitle

{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $inspections = DB::table('appraisal_areas')
                        ->join('areas','appraisal_areas.area_id','=','areas.id')
                        ->select(
                            'areas.name AS name',
                            'appraisal_areas.dimension AS dimension'
                        )
                        ->where('appraisal_areas.appraisal_request_id','=',$this->id)
                        ->get();

        return $inspections;

    }

    // public function map($item): array
    // {
    //     return [
    //         $item->id,
    //         $item->applicant_name,
    //         $item->applicant_email
    //     ];
    // }


    public function headings(): array
    {
        return [
            'Area',
            'Dimensión'
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Areas';
    }
}
