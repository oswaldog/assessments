<?php

namespace App\Exports;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;


class LevelsCoatingsExport implements 
FromCollection, 
ShouldAutoSize, 
// WithMapping, 
WithHeadings,
WithStyles,
WithTitle

{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }


    public function collection()
    {
        $levels =   DB::table('coatings_by_levels_view')
                        ->select(
                            'level',
                            'ventanas',
                            'pisos',
                            'pared_cocina',
                            'topes_cocina',
                            'pared_sanitarios',
                            'pisos_sanitarios',
                            'techos',
                            'techo_sanitario',
                            'closets',
                            'jambas_puertas',
                            'walking_closets',
                            'gabinetes_cocina',
                            'puerta_principal',
                            'puertas_interiores',
                            'ducha_sanitario')
                        ->where('appraisal_request_id','=',$this->id)
                        ->get();    
                        
        return $levels;
    }

    public function headings():array
    {
        return [
            'Nivel',
            'Ventanas',
            'Pisos',
            'Pared Cocina',
            'Topes Cocina',
            'Pared Sanitarios',
            'Pisos Sanitarios',
            'Techos',
            'Techo Sanitario',
            'Closets',
            'Jambas Puertas',
            'Walking Closets',
            'Gabinetes Cocina',
            'Puerta Principal',
            'Puertas Interiores',
            'Ducha Sanitario'            
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Revestimientos';
    }    
}
