<?php

namespace App\Exports;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
// use Maatwebsite\Excel\Concerns\WithMapping;


class InspectionsEnvironmentExport implements 
    FromCollection, 
    ShouldAutoSize, 
    // WithMapping, 
    WithHeadings,
    WithStyles,
    WithTitle

{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $inspections = DB::table('inspections_view')
                        ->select(
                            'clase_social',
                            'mayor_uso',
                            'condicion_calles',
                            'asceras',
                            'contenes',
                            'agua_tuberia',
                            'electricidad',
                            'drenaje_pluvial',
                            'plazas_comerciales',
                            'farmacias',
                            'supermercado',
                            'estacion_combustible',
                            'restaurants',
                            'gimnasio_entorno',
                            'estacion_metro',
                            'facil_transporte_publico',
                            'clinicas',
                            'hospital_publico',
                            'trafico_complicado',
                            'recogida_basura',
                            'internet',
                            'cable',
                            'telefonia',
                            'lindero_norte',
                            'lindero_sur',
                            'lindero_este',
                            'lindero_oeste',
                            'distancia_escuelas',
                            'distancia_transporte_publico',
                            'distancia_comercios',
                            'distancia_centro_ciudad',
                            'comentarios'
                        )
                        ->where('appraisal_request_id','=',$this->id)
                        ->get();


        return $inspections;

    }

    // public function map($item): array
    // {
    //     return [
    //         $item->id,
    //         $item->applicant_name,
    //         $item->applicant_email
    //     ];
    // }


    public function headings(): array
    {
        return [
            'Clase social del entorno',
            'Mejor y mas alto uso',
            'Calles ',
            'Asceras',
            'Contenes',
            'Agua por tuberia',
            'Electricidad',
            'Drenaje pluvial',
            'Plazas comerciales',
            'Farmacias',
            'Supermercado',
            'Estacion de combustible',
            'Restaurants',
            'Gimnasio',
            'Estacion de metro',
            'Facil acceso a transporte publico',
            'Clinicas',
            'Hospital publico',
            'Trafico complicado',
            'Recogida de basura',
            'Internet',
            'Cable',
            'Telefonía ',
            'Lindero Norte',
            'Lindero Sur',
            'Lindero Este',
            'Lindero Oeste',
            'Distancia a escuelas',
            'Distancia a transporte público',
            'Distancia a comercios',
            'Distancia a la ciudad',
            'Comentarios'
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Entorno';
    }
}
