<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class InspectionsMultiSheetExport implements WithMultipleSheets
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function sheets(): array
    {
        $sheets = [];

        $sheets[0] = new AppraisalRequestsExport($this->id);
        $sheets[1] = new InspectionsInfoGeneralExport($this->id);
        $sheets[2] = new LevelsInternalDistributionsExport($this->id);
        $sheets[3] = new LevelsCoatingsExport($this->id);
        $sheets[4] = new InspectionsAmenitiesExport($this->id);
        $sheets[5] = new InspectionsEnvironmentExport($this->id);
        $sheets[6] = new InspectionsAreaExport($this->id);
        

        return $sheets;
    }


}
