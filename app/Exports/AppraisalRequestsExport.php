<?php

namespace App\Exports;

use App\AppraisalRequest;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;
// use Maatwebsite\Excel\Concerns\WithMapping;

class AppraisalRequestsExport implements 
    FromCollection, 
    ShouldAutoSize, 
    // WithMapping, 
    WithHeadings,
    WithStyles,
    WithTitle
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $appraisal_request = DB::table('appraisal_requests_rpt_view')
                                ->select(
                                'applicant_name',
                                'applicant_phone',
                                'applicant_email',
                                'contact_name',
                                'contact_phone',
                                'delivery_type_name',
                                'bank_name',
                                'amount',
                                'payment_method_name',
                                'payment_status',
                                'inspection_date',
                                'inspection_time',
                                'delivery_date',
                                'inspector_name',
                                'address',
                                'comments',
                                'property_type_name',
                                'property_use_status_name',
                                'property_number',
                                'property_levels',
                                'construction_year',
                                'geo_coordinates',
                                'cadastral_number',
                                'registration_number',
                                'book_number',
                                'folio_number',
                                'expedition_date',
                                'property_status',
                                'owner',
                                'land_area',
                                'improvement_area',
                                'residence',
                                'address_number',
                                'address_street',
                                'sector_name',
                                'city_name',
                                'municipality_name', 
                                'province_name'
                                )
                                ->where('appraisal_request_id','=',$this->id)
                                ->get();

        return $appraisal_request;

    }

    public function headings(): array
    {
        return [
            'Nombre Solicitante',
            'Teléfono Solicitante',
            'Email Solicitante',
            'Persona Contacto',
            'Teléfono Contacto',
            'Tipo de Entrega',
            'Banco',
            'Monto a cobrar',
            'Forma de Pago',
            'Estatus del Pago',
            'Fecha Inspección',
            'Hora Inspección',
            'Fecha Entrega',
            'Inspector',
            'Dirección',
            'Comentarios',
            'Tipo de Inmueble',
            'Ocupada por',
            'Nro del Inmueble',
            'Niveles del Inmueble',
            'Año de construcción del Inmueble',
            'Coordenadas Geográficas',
            'Designación Catastral',
            'Matricula / Titulo N°',
            'Nro de Libro',
            'Nro de Folio',
            'Fecha Expedición',
            'Condición del Inmueble',
            'Propietario',
            'Área del solar',
            'Área de Mejora',
            'Residencia',
            'Número',
            'Calle',
            'Sector',
            'Ciudad',
            'Municipio',
            'Provincia'
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Solicitud';
    }    
}
