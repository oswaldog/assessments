<?php

namespace App\Exports;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithTitle;


class LevelsInternalDistributionsExport implements 
FromCollection, 
ShouldAutoSize, 
// WithMapping, 
WithHeadings,
WithStyles,
WithTitle

{

    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function collection()
    {
        $levels =   DB::table('internal_data_view')
                        ->select(
                            'level',
                            'recibidor',
                            'sala',
                            'comedor',
                            'cocina',
                            'cocina_fria_caliente',
                            'cocina_desayunador',
                            'cocina_isla',
                            'area_lavado',
                            'cuarto_servicio',
                            'sanitario_servicio',
                            'terraza',
                            'patio',
                            'medio_sanitario',
                            'dormitorios_grandes',
                            'dormitorios_medianos',
                            'dormitorios_pequenos',
                            'sanitarios',
                            'closets',
                            'walking_closets',
                            'closet_desahogo',
                            'sala_estar',
                            'balcon'
                            )
                        ->where('appraisal_request_id','=',$this->id)
                        ->get();    
                        
        return $levels;
    }

    public function headings():array
    {
        return [
            'Nivel',
            'Recibidor',
            'Sala',
            'Comedor',
            'Cocina',
            'Cocina Fría/Caliente',
            'Cocina con Desayunador',
            'Cocina con Isla',
            'Área de Lavado',
            'Cuarto de Servicio',
            'Baño de Servicio',
            'Terraza',
            'Patio',
            '1/2 Baño',
            'Habitaciones Grandes',
            'Habitaciones Medianas',
            'Habitaciones Pequeñas',
            'Baños',
            'Closets',
            'Walking Closets',
            'Closet de Desahogo',
            'Sala de Estar',
            'Balcón'                        
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Distribución Interna';
    }  
}
