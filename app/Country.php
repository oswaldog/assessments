<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model 
{

    protected $table = 'countries';
    public $timestamps = false;
    protected $fillable = ['name','code'];


    public function provinces()
    {
        return $this->hasMany('App\Province');
    }

}