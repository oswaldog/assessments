<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model 
{

    protected $table = 'cities';
    public $timestamps = false;
    protected $fillable = ['name','municipality_id'];

    public function municipality()
    {
        return $this->belongsTo('App\Municipality');
    }

    public function sectors()
    {
        return $this->hasMany('App\Sector');
    }

}