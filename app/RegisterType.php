<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterType extends Model
{
    //
    protected $table = 'register_types';
}
