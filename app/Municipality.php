<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model 
{

    protected $table = 'municipalities';
    public $timestamps = false;
    protected $fillable = ['name','province_id'];

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function Cities()
    {
        return $this->hasMany('App\City');
    }


}