<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalRequest extends Model 
{

    protected $table = 'appraisal_requests';
    public $timestamps = true;

    

    public function property()
    {
        return $this->hasOne('App\Property','appraisal_request_id');
    }

    public function attachments()
    {
        return $this->hasMany('App\Attachment','appraisal_request_id');
        
    }
 
    public function areas()
    {
        return $this->hasMany('App\AppraisalArea','appraisal_request_id');
    }


}