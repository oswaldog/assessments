<?php

return [

    'verified' => 'Su correo electrónico ha sido verificado!',
    'invalid' => 'El enlace de verificación no es válido.',
    'already_verified' => 'El correo electrónico ya está verificado.',
    'user' => 'No podemos encontrar un usuario con esa dirección de correo electrónico.',
    'sent' => '¡Hemos enviado su enlace de verificación por correo electrónico!',

];
