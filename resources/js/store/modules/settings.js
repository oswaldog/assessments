import * as types from '../mutation-types'

// state
export const state = {
  keyWorkTeamShowMembers: 0,
  keyWorkTeamAddMembers: 0,
}

// getters
export const getters = {
    keyWorkTeamShowMembers: state => state.keyWorkTeamShowMembers,
    keyWorkTeamAddMembers: state => state.keyWorkTeamAddMembers,
}

// mutations
export const mutations = {
  [types.RESET_KEY_WORKTEAM_SHOW_MEMBERS] (state) {
    state.keyWorkTeamShowMembers += 1
  },

  [types.RESET_KEY_WORKTEAM_ADD_MEMBERS] (state) {
    state.keyWorkTeamAddMembers += 1
  },

}

// actions
export const actions = {
  resetKeyWorkTeamShowMembers ({ commit }) {
    commit(types.RESET_KEY_WORKTEAM_SHOW_MEMBERS)
  },
  resetKeyWorkTeamAddMembers ({ commit }) {
    commit(types.RESET_KEY_WORKTEAM_ADD_MEMBERS)
  },
 
}
