import * as types from '../mutation-types'

// state
export const state = {
  comparableId: '',
  modalPhoto: false,
  modalInfo: false,
  comparables: []
}

// getters
export const getters = {
    comparableId: state => state.comparableId,
    modalPhoto: state => state.modalPhoto,
    modalInfo: state => state.modalInfo,
    comparables: state => state.comparables
}

// mutations
export const mutations = {
  [types.SET_COMPARABLE_ID] (state, { value }) {
    state.comparableId = value
  },
  [types.SHOW_MODAL_PHOTOS] (state, { value }) {
      state.modalPhoto = value
  },
  [types.SHOW_MODAL_INFO] (state, { value }) {
      state.modalInfo = value
  },
  [types.ADD_COMPARABLE] (state, { value }){
    state.comparables.push(value)
  },
  [types.REMOVE_COMPARABLE] (state, { value }){
    const tmpArr = [...state.comparables]
    state.comparables = tmpArr.filter(e => e !== value)
  },


}

// actions
export const actions = {
  setComparableId ({ commit }, payload) {
    commit(types.SET_COMPARABLE_ID, payload)
  },
  showModalPhotos ({ commit }, payload) {
    commit(types.SHOW_MODAL_PHOTOS, payload)
  },
  showModalInfo ({ commit }, payload) {
    commit(types.SHOW_MODAL_INFO, payload)
  },
  addComparable ({ commit }, payload) {
    commit(types.ADD_COMPARABLE, payload)
  },
  removeComparable ({ commit }, payload) {
    commit(types.REMOVE_COMPARABLE, payload)
  }

}
