import * as types from "../mutation-types";

// state
export const state = {
  keySolicitudes: 0,
  keyDatatable: 0,
  keyInspection: 0,
  keyPropertyInfo: 0,
  hasInspection: false,
  itemDatatable: [],
  modalDocuments: false,
  keyModalDocuments: 0,
  modalImagesGallery: false,
  keyModalImagesGallery: 0,
  totalImages: 0,
  totalDocuments: 0,
  imagesGallerySelected: [],
  workteamInfo: {},
  registerType: 0
};

// getters
export const getters = {
  keySolicitudes: state => state.keySolicitudes,
  keyDatatable: state => state.keyDatatable,
  keyInspection: state => state.keyInspection,
  keyPropertyInfo: state => state.keyPropertyInfo,
  hasInspection: state => state.hasInspection,
  itemDatatable: state => state.itemDatatable,
  modalDocuments: state => state.modalDocuments,
  keyModalDocuments: state => state.keyModalDocuments,
  modalImagesGallery: state => state.modalImagesGallery,
  keyModalImagesGallery: state => state.keyModalImagesGallery,
  totalImages: state => state.totalImages,
  totalDocuments: state => state.totalDocuments,
  imagesGallerySelected: state => state.imagesGallerySelected,
  workteamInfo: state => state.workteamInfo,
  registerType: state => state.registerType
};

// mutations
export const mutations = {
  [types.RESET_KEY_SOLICITUDES](state) {
    state.keySolicitudes += 1;
  },

  [types.RESET_KEY_DATATABLE](state) {
    state.keyDatatable += 1;
  },

  [types.RESET_KEY_PROPERTY_INFO](state) {
    state.keyPropertyInfo += 1;
  },

  [types.RESET_KEY_INSPECCION](state) {
    state.keyInspection += 1;
  },

  [types.SET_HAS_INSPECTION](state, { value }) {
    state.hasInspection = value;
  },

  [types.FETCH_ITEM_SELECTED](state, { item }) {
    state.itemDatatable = item;
  },

  [types.RESET_ITEM_SELECTED](state) {
    state.itemDatatable = [];
  },

  [types.SHOW_MODAL_DOCUMENTS](state, { value }) {
    state.modalDocuments = value;
  },

  [types.RESET_KEY_MODAL_DOCUMENTS](state) {
    state.keyModalDocuments += 1;
  },

  [types.SHOW_MODAL_IMAGES_GALLERY](state, { value }) {
    state.modalImagesGallery = value;
  },

  [types.RESET_KEY_MODAL_IMAGES_GALLERY](state) {
    state.keyModalImagesGallery += 1;
  },

  [types.ADD_IMAGES_GALLERY_SELECTED](state, { item }) {
    state.imagesGallerySelected.push(item);
  },

  [types.REMOVE_IMAGES_GALLERY_SELECTED](state, { item }) {
    if (state.imagesGallerySelected.includes(item)) {
      const findIndex = state.imagesGallerySelected.indexOf(item);
      state.imagesGallerySelected.splice(findIndex, 1);
    }
  },

  [types.RESET_IMAGES_GALLERY_SELECTED](state) {
    state.imagesGallerySelected = [];
  },

  [types.SET_TOTAL_IMAGES](state, { value }) {
    state.totalImages = value;
  },

  [types.SET_TOTAL_DOCUMENTS](state, { value }) {
    state.totalDocuments = value;
  },

  [types.UPDATE_WORKTEAM_INFO](state, { value }) {
    state.workteamInfo = value;
  },

  [types.SET_REGISTER_TYPE](state, { value }) {
    state.registerType = value;
  }
};

// actions
export const actions = {
  resetKeySolicitudes({ commit }) {
    commit(types.RESET_KEY_SOLICITUDES);
  },
  resetKeyPropertyInfo({ commit }) {
    commit(types.RESET_KEY_PROPERTY_INFO);
  },
  resetKeyDatatable({ commit }) {
    commit(types.RESET_KEY_DATATABLE);
  },
  resetkeyInspection({ commit }) {
    commit(types.RESET_KEY_INSPECCION);
  },
  setHasInspection({ commit }, payload) {
    commit(types.SET_HAS_INSPECTION, payload);
  },
  fetchItemSelected({ commit }, payload) {
    commit(types.FETCH_ITEM_SELECTED, payload);
  },
  resetItemSelected({ commit }) {
    commit(types.RESET_ITEM_SELECTED);
  },
  showModalDocuments({ commit }, payload) {
    commit(types.SHOW_MODAL_DOCUMENTS, payload);
  },
  resetkeyModalDocuments({ commit }) {
    commit(types.RESET_KEY_MODAL_DOCUMENTS);
  },
  showModalImagesGallery({ commit }, payload) {
    commit(types.SHOW_MODAL_IMAGES_GALLERY, payload);
  },
  resetkeyModalImagesGallery({ commit }) {
    commit(types.RESET_KEY_MODAL_IMAGES_GALLERY);
  },
  setTotalImages({ commit }, payload) {
    commit(types.SET_TOTAL_IMAGES, payload);
  },
  addImagesGallerySelected({ commit }, payload) {
    commit(types.ADD_IMAGES_GALLERY_SELECTED, payload);
  },
  removeImagesGallerySelected({ commit }, payload) {
    commit(types.REMOVE_IMAGES_GALLERY_SELECTED, payload);
  },
  resetImagesGallerySelected({ commit }) {
    commit(types.RESET_IMAGES_GALLERY_SELECTED);
  },
  setTotalDocuments({ commit }, payload) {
    commit(types.SET_TOTAL_DOCUMENTS, payload);
  },
  updateWorkTeamInfo({ commit }, payload) {
    commit(types.UPDATE_WORKTEAM_INFO, payload);
  },
  setRegisterType({ commit }, payload) {
    commit(types.SET_REGISTER_TYPE, payload);
  }
};
