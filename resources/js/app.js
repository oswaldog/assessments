import Vue from 'vue'
import store from '~/store'
import InputFacade from 'vue-input-facade'

import Vuelidate from 'vuelidate'
import router from '~/router'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import i18n from '~/plugins/i18n'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faRestroom, faBed, faLayerGroup, faCar  } from '@fortawesome/free-solid-svg-icons' 
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faRestroom,faBed,faLayerGroup,faCar)

Vue.component('font-awesome-icon', FontAwesomeIcon)


import App from '~/components/App'

import '~/plugins'
import '~/components'

const moment = require('moment')
require('moment/locale/es')

Vue.config.productionTip = false

Vue.use(Vuelidate)
Vue.use(InputFacade)

Vue.use(require('vue-moment'), {
  moment
})



/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
