

const commonFunc = (function () {

    return {
        cadastralIdToGeocode(cad)
        { 
     
            let A2,A3,C2,C3,A5,A6,A8,A9,A10,A12,A14,A15,A17,A18,A19,A21,A23,A25,A26,
                A28,A29,A30,A32,A33,A34,A36,D17,D18,D19,D21,D22,D24,D25,D27,D29,D30,
                D31,D32,D33,D34,D35,D36,D37,D38,D39,D40,M1,M2,M3;
            
            A2 = ("19Q"+" "+cad.substring(0,1)+cad.substring(2,3)+
            cad.substring(4,5)+cad.substring(6,7)+cad.substring(8,9)+
            cad.substring(10,11));
      
            A3 = ("UTM"+" "+"2"+cad.substring(1,2)+cad.substring(3,4)+
            " "+cad.substring(5,6)+cad.substring(7,8)+cad.substring(9,10)+
            cad.substring(11,12)); 
      
            C2 = (cad.substring(0,1)+cad.substring(2,3)+
            cad.substring(4,5)+cad.substring(6,7)+cad.substring(8,9)+
            cad.substring(10,11));
      
            C3 = ("2"+cad.substring(1,2)+cad.substring(3,4)+
            cad.substring(5,6)+cad.substring(7,8)+cad.substring(9,10)+
            cad.substring(11,12));
      
            A5 = (6378137);
            A6 = (6356752.31424518);
            A8 = (0.0818191908426203); 
            A9 = (0.0820944379496945);
            A10= (Math.pow(A9,2));
            A12= (Math.pow(A5,2)/A6);
            A14= (C2-500000);
            A15= (C3);
            A17= ((6*19)-183);
            A18= (A15/(6366197.724*0.9996));
            A19= (Math.pow((Math.cos(A18)),2));
            A21= (A12/Math.pow((1+A10*A19),0.5)*0.9996);
            A23= (A14/A21);
            A25= Math.sin(2*A18)
            A26= (A25*A19);
            A28= (A18+(A25/2));
            A29= ((3*A28+A26)/4);
            A30= ((5*A29+A26*A19)/3);
            A32= ((3/4)*A10);
            A33= ((5/3)*Math.pow(A32,2));
            A34= ((35/27)*Math.pow(A32,3));
            A36= (0.9996*A12*(A18-A32*A28+A33*A29-A34*A30));
      
            D17= ((A15-A36)/A21);
            D18= (((A10*Math.pow(A23,2))/2)*A19);
            D19= (A23*(1-(D18/3)));
            D21= (D17*(1-D18)+A18);
            D22= (((Math.exp(D19))-(Math.exp(-D19)))/2); 
            D24= (Math.atan(D22/Math.cos(D21)));
            D25= (Math.atan(Math.cos(D24)*Math.tan(D21)));
            D27= (((D24/Math.PI)*180)+A17);
            D29= (((A18+(1+A10*A19-((3/2)*A10*(Math.sin(A18)*Math.cos(A18)
                  *(D25-A18))))*(D25-A18))/Math.PI)*180);
            D30=(D27*-1);
      
            D31= (new String(D29)).substring(0,11);
            D32= (new String(D30)).substring(0,11);
      
            if (D29<0){ 
                D33= Math.ceil(D29);
                D34= Math.ceil(D30);
            }else{
                D33= Math.floor(D29);
                D34= Math.floor(D30);
            }
      
            D35= ((D29-D33)*60);
            D36= ((D30-D34)*60);
      
            if (D35<0){ 
                D37= Math.ceil(D35);
                D38= Math.ceil(D36);
            }else{
                D37= Math.floor(D35);
                D38= Math.floor(D36);
            }
      
            D39= (Math.round(((D35-D37)*60)*100)/100);
            D40= (Math.round(((D36-D38)*60)*100)/100);
      
            M1=" º";
            M2=" '"
            M3=" ''"
            /*
            //COORDENADAS UTM
            console.log(A2);  //(X)
            console.log(A3);  //(Y)
      
            //COORDENADAS GEOGRÁFICAS
            console.log(D31);  //(N) LONGITUD
            console.log(D32);  //(W) LATITUD
      
            //COORDENADAS EN GRADOS-MIN-SEG
            //LATITUD
            console.log(D33+M1); 
            console.log(D37+M2);
            console.log(D39+M3);
            //LONGITUD
            console.log(D34+M1);
            console.log(D38+M2);
            console.log(D40+M3);
          */
    
            return D31+',-'+D32;
        },

        async getLocation() {   

            const options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };
      
            function success(position) {
      
              return position;
      
            }
      
            function error(err) {
      
              let errMessage;
      
              switch (err.code) {
                case 1:
                  errMessage = 'Este sitio no tiene permisos para que pueda determinarse su geolocalización.';
                  break;
                case 2:
                  errMessage = 'Una fuente interna de la geolocalización retornó error.';
                  break;
                case 3:
                  errMessage = 'El tiempo permitido para efectuar la petición se ha agotado.';
                  break;
                default:
                  errMessage = 'Por favor intente nuevamente. Si el problema persiste contacte al Administrador';
              }
              
              err.customMessage = errMessage;
              return err;
      
            }
            
            return new Promise((resolve, reject) => {
      
              if(!("geolocation" in navigator)) {
                reject(new Error('Geolocation is not available.'));
              }
      
              navigator.geolocation.getCurrentPosition(
                  pos => {
                    resolve(success(pos));
                  }, 
                  err => {
                    reject(error(err));
                  },
                  options
              );
      
            });        
      
          },

          formatRealNumber(param){
            if (!param) {
                return '0.00' 
            }
            return Number.parseFloat(param).toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")
        },
        
    }
    
})();


export { commonFunc };