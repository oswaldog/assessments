<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW active_permissions_view AS
                            SELECT t.id, t.owner_user_id, t.member_user_id,array_remove(ARRAY[t.administrative,t.inspector,t.reviewer,t.approver],NULL) AS array_values
                            FROM (
                            SELECT
                            id, 
                            owner_user_id,
                            member_user_id,
                            CASE WHEN (permissions -> 0 ->> 'status')::boolean = 'true' THEN 0 END::CHAR AS administrative,
                            CASE WHEN (permissions -> 1 ->> 'status')::boolean = 'true' THEN 1 END::CHAR AS inspector,
                            CASE WHEN (permissions -> 2 ->> 'status')::boolean = 'true' THEN 2 END::CHAR AS reviewer,
                            CASE WHEN (permissions -> 3 ->> 'status')::boolean = 'true' THEN 3 END::CHAR AS approver
                            FROM work_teams) AS t ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW active_permissions_view");
    }
}
