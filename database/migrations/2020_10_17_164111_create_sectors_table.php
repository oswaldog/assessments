<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('sectors', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 150);
			$table->integer('city_id')->unsigned();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		DB::statement('drop table IF exists sectors cascade');
	}
}
