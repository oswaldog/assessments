<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AppraisalStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('status');

        Schema::dropIfExists('appraisal_status');
        
        Schema::create('appraisal_status', function (Blueprint $table) {
			$table->increments('id');
            $table->string('name', 150);
            $table->string('description', 150);
        });

        Schema::table('appraisal_requests', function (Blueprint $table) {
            $table->bigInteger('appraisal_status_id')->unsigned();
            $table->foreign('appraisal_status_id')->references('id')->on('appraisal_status')
            ->onDelete('restrict')
            ->onUpdate('restrict');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
