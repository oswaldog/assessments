<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoatingMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coating_materials', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('coating_id')->unsigned();
            $table->foreign('coating_id')->references('id')->on('coatings')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->string('name',50);
            $table->string('text_value',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coating_materials');
    }
}
