<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBancosTable extends Migration {

	public function up()
	{
		Schema::create('bancos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 50);
		});
	}

	public function down()
	{
		Schema::dropIfExists('bancos');
	}
}