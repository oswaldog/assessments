<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewForeignKeysTableSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->unsignedBigInteger('tipo_entrega_id')->comment('ID del tipo de entrega (FK)');
            $table->foreign('tipo_entrega_id')->references('id')->on('tipo_entrega');
            $table->unsignedBigInteger('forma_pago_id')->comment('ID del tipo de forma de pago (FK)');;
            $table->foreign('forma_pago_id')->references('id')->on('forma_pago');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            //
        });
    }
}
