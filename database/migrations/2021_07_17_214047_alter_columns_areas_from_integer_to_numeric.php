<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnsAreasFromIntegerToNumeric extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS comparables_view");
        DB::statement("DROP VIEW IF EXISTS inspections_view");
        DB::statement("DROP VIEW IF EXISTS appraisal_requests_view");
        DB::statement('DROP VIEW IF EXISTS appraisal_requests_rpt_view');

        Schema::table('properties', function(Blueprint $table) {
            $table->decimal('land_area',15,2)->default('0')->change();
            $table->decimal('improvement_area',15,2)->default('0')->change();
        });

        Schema::table('appraisal_areas', function (Blueprint $table) {
            $table->decimal('dimension',15,2)->default('0')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
