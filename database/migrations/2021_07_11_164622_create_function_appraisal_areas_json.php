<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFunctionAppraisalAreasJson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE OR REPLACE FUNCTION func_appraisal_areas_json(appraisal_request_id BIGINT)
            RETURNS JSONB AS $FUNC$ 
            
            SELECT jsonb_agg(result_query) FROM (
                SELECT appraisal_request_id, area_id, name, dimension
                FROM appraisal_areas
                INNER JOIN areas
                    ON areas.id = appraisal_areas.area_id
                WHERE
                    appraisal_request_id = $1
                ) AS result_query
            $FUNC$ LANGUAGE SQL;
            ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP FUNCTION IF EXISTS func_appraisal_areas_json(BIGINT)");    
    }
}
