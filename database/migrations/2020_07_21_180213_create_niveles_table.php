<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNivelesTable extends Migration {

	public function up()
	{
		Schema::create('niveles', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('inspeccion_id')->unsigned();
			$table->integer('nivel');
			$table->json('data');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('niveles');
	}
}