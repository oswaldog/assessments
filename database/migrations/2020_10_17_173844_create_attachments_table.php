<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appraisal_request_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->text('path')->nullable();
            $table->text('relative_path')->nullable();
            $table->string('name',150)->nullable();
            $table->string('type',150)->nullable();
            $table->integer('size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop table IF exists attachments cascade');
    }
}
