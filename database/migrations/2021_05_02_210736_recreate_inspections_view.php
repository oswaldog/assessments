<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecreateInspectionsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW inspections_view AS
                        SELECT 
                            t1.id AS appraisal_request_id,
                            t2.id AS property_id,
                            t1.inspection_date, 
                            t1.delivery_date, 
                            t1.applicant_name, 
                            t1.address,
                            t2.property_number,
                            t6.name AS province,
                            t2.owner,
                            'Matricula' AS document_type,
                            t2.registration_number,
                            t2.cadastral_number,
                            t2.folio_number,
                            t2.book_number,
                            t2.expedition_date,
                            t2.construction_year,
                            t2.property_levels,
                            t7.name AS property_use_status,
                            t2.land_area,
                            t2.improvement_area,
                            CASE WHEN t2.general_data->'entorno'->'agua_tuberia'->>'value' = 'true' THEN 'SI' ELSE 'NO' END AS agua_tuberia,
                            CASE WHEN t2.general_data->'entorno'->'asceras'->>'value' = 'true' THEN 'SI' ELSE 'NO' END AS asceras,
                            CASE WHEN t2.general_data->'entorno'->'cable'->>'value' = 'true' THEN 'SI' ELSE 'NO' END AS cable,
                            t2.general_data->'entorno'->'clase_social'->>'value' AS clase_social,
                            CASE WHEN t2.general_data->'entorno'->'clinicas'->>'value' = 'true' THEN 'SI' ELSE 'NO' END AS clinicas,
                            t2.general_data->'entorno'->'comentarios'->>'value' AS comentarios,
                            t2.general_data->'entorno'->'condicion_calles'->>'value' AS condicion_calles,
                            CASE WHEN t2.general_data->'entorno'->'contenes'->>'value' = 'true' THEN 'SI' ELSE 'NO' END AS contenes,
                            t2.general_data->'entorno'->'distancia_centro_ciudad'->>'value' AS distancia_centro_ciudad,
                            t2.general_data->'entorno'->'distancia_comercios'->>'value' AS distancia_comercios,
                            t2.general_data->'entorno'->'distancia_escuelas'->>'value' AS distancia_escuelas,
                            t2.general_data->'entorno'->'distancia_transporte_publico'->>'value' AS distancia_transporte_publico,
                            CASE WHEN t2.general_data->'entorno'->'drenaje_pluvial'->>'value' = 'true' THEN 'SI' ELSE 'NO' END AS drenaje_pluvial,
                            CASE WHEN t2.general_data->'entorno'->'electricidad'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS electricidad,
                            CASE WHEN t2.general_data->'entorno'->'estacion_combustible'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS estacion_combustible,
                            CASE WHEN t2.general_data->'entorno'->'estacion_metro'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS estacion_metro,
                            CASE WHEN t2.general_data->'entorno'->'facil_transporte_publico'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS facil_transporte_publico,
                            CASE WHEN t2.general_data->'entorno'->'farmacias'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS farmacias,
                            CASE WHEN t2.general_data->'entorno'->'gimnasio'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS gimnasio_entorno,
                            CASE WHEN t2.general_data->'entorno'->'hospital_publico'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS hospital_publico,
                            CASE WHEN t2.general_data->'entorno'->'internet'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS internet,
                            CASE WHEN t2.general_data->'entorno'->'lindero_este'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS lindero_este,
                            CASE WHEN t2.general_data->'entorno'->'lindero_norte'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS lindero_norte,
                            CASE WHEN t2.general_data->'entorno'->'lindero_oeste'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS lindero_oeste,
                            CASE WHEN t2.general_data->'entorno'->'lindero_sur'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS lindero_sur,
                            CASE WHEN t2.general_data->'entorno'->'mayor_uso'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS mayor_uso,
                            CASE WHEN t2.general_data->'entorno'->'plazas_comerciales'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS plazas_comerciales,
                            CASE WHEN t2.general_data->'entorno'->'recogida_basura'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS recogida_basura,
                            CASE WHEN t2.general_data->'entorno'->'restaurants'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS restaurants,
                            CASE WHEN t2.general_data->'entorno'->'supermercado'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS supermercado,
                            CASE WHEN t2.general_data->'entorno'->'telefonia'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS telefonia,
                            CASE WHEN t2.general_data->'entorno'->'trafico_complicado'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS trafico_complicado,
                            CASE WHEN t2.general_data->'amenidades'->'sauna'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS sauna,
                            CASE WHEN t2.general_data->'amenidades'->'gazebo'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS gazebo,
                            CASE WHEN t2.general_data->'amenidades'->'jacuzzi'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS jacuzzi,
                            CASE WHEN t2.general_data->'amenidades'->'picuzzi'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS picuzzi,
                            CASE WHEN t2.general_data->'amenidades'->'piscina'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS piscina,
                            CASE WHEN t2.general_data->'amenidades'->'gimnasio'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS gimnasio_amenidades,
                            CASE WHEN t2.general_data->'amenidades'->'campo_golf'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS campo_golf,
                            CASE WHEN t2.general_data->'amenidades'->'area_juegos'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS area_juegos,
                            CASE WHEN t2.general_data->'amenidades'->'canchas_tenis'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS canchas_tenis,
                            CASE WHEN t2.general_data->'amenidades'->'sala_choferes'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS sala_choferes,
                            CASE WHEN t2.general_data->'amenidades'->'salon_reuniones'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS salon_reuniones,
                            CASE WHEN t2.general_data->'amenidades'->'terraza_techada'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS terraza_techada,
                            CASE WHEN t2.general_data->'amenidades'->'canchas_baloncesto'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS canchas_baloncesto,
                            CASE WHEN t2.general_data->'amenidades'->'terraza_destechada'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS terraza_destechada,
                            CASE WHEN t2.general_data->'informacion_general'->'pozo'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS pozo,
                            CASE WHEN t2.general_data->'informacion_general'->'patio'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS patio,
                            CASE WHEN t2.general_data->'informacion_general'->'galeria'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS galeria,
                            t2.general_data->'informacion_general'->'ascensor'->>'value' AS ascensor,
                            CASE WHEN t2.general_data->'informacion_general'->'cisterna'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS cisterna,
                            CASE WHEN t2.general_data->'informacion_general'->'intercom'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS intercom,
                            t2.general_data->'informacion_general'->'shutters'->>'value' AS shutters,
                            CASE WHEN t2.general_data->'informacion_general'->'aa_central'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS aa_central,
                            CASE WHEN t2.general_data->'informacion_general'->'marquesina'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS marquesina,
                            CASE WHEN t2.general_data->'informacion_general'->'planta_full'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS planta_full,
                            CASE WHEN t2.general_data->'informacion_general'->'gas_tubería'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS gas_tubería,
                            CASE WHEN t2.general_data->'informacion_general'->'areas_comunes'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS areas_comunes,
                            CASE WHEN t2.general_data->'informacion_general'->'entrada_lobby'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS entrada_lobby,
                            t2.general_data->'informacion_general'->'ascensor_carga'->>'value' AS ascensor_carga,
                            CASE WHEN t2.general_data->'informacion_general'->'herraje_ventanas'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS herraje_ventanas,
                            t2.general_data->'informacion_general'->'parqueos_techados'->>'value' AS parqueos_techados,
                            CASE WHEN t2.general_data->'informacion_general'->'porton_electrico'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS porton_electrico,
                            CASE WHEN t2.general_data->'informacion_general'->'sistema_incendio'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS sistema_incendio,
                            CASE WHEN t2.general_data->'informacion_general'->'verja_perimetral'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS verja_perimetral,
                            CASE WHEN t2.general_data->'informacion_general'->'camaras_seguridad'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS camaras_seguridad,
                            COALESCE (t2.general_data->'informacion_general'->'parqueos_aire_libre'->>'value','0') AS parqueos_aire_libre,
                            COALESCE (t2.general_data->'informacion_general'->'niveles_estructura'->>'value','0') AS niveles_estructura,
                            COALESCE (t2.general_data->'informacion_general'->'condiciones_generales'->>'value','0') AS condiciones_generales,
                            CASE WHEN t2.general_data->'informacion_general'->'herraje_puerta_principal'->>'value'  = 'true' THEN 'SI' ELSE 'NO' END AS herraje_puerta_principal
                        FROM appraisal_requests AS t1
                        INNER JOIN properties AS t2
                            ON t1.id = t2.appraisal_request_id
                        INNER JOIN sectors AS t3
                            ON t2.sector_id = t3.id
                        INNER JOIN cities AS t4
                            ON t3.city_id = t4.id
                        INNER JOIN municipalities AS t5
                            ON t4.municipality_id = t5.id	
                        INNER JOIN provinces AS t6
                            ON t5.province_id = t6.id
                        INNER JOIN property_use_status AS t7
                            ON t2.property_use_status_id = t7.id
                        INNER JOIN property_types AS t8
                            ON t2.property_type_id = t8.id
	                 ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS inspections_view");
    }
}
