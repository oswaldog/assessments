<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkTeamsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW work_teams_view AS
                            SELECT
                            work_teams.id,
                            work_teams.owner_user_id,
                            owner_users.name AS owner_user_name,
                            work_teams.member_user_id, 
                            member_users.name AS member_user_name,
                            (permissions -> 0 ->> 'status')::BOOLEAN AS is_administrative,
                            (permissions -> 1 ->> 'status')::BOOLEAN AS is_inspector,
                            (permissions -> 2 ->> 'status')::BOOLEAN AS is_evaluator,
                            (permissions -> 3 ->> 'status')::BOOLEAN AS is_approver
                            FROM work_teams
                            JOIN users AS owner_users
                                ON work_teams.owner_user_id = owner_users.id
                            JOIN users AS member_users
                                ON work_teams.member_user_id = member_users.id
	                 ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS work_teams_view");
    }
}
