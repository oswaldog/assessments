<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachmentsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW attachments_view AS
                        SELECT
                        id,
                        appraisal_request_id,
                        user_id,
                        split_part(relative_path, '/', 4) AS file_type,
                        path,
                        relative_path,
                        name,
                        real_name,
                        type AS file_format,
                        pg_size_pretty(size::bigint) AS size,
                        pos
                        FROM attachments
	                 ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS attachments_view");
    }
}
