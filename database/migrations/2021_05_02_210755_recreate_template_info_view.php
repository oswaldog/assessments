<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecreateTemplateInfoView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS template_info_view");
        DB::statement("
        CREATE OR REPLACE VIEW template_info_view AS
            SELECT 
            properties.appraisal_request_id AS template_id,
            properties.id AS property_id,
            properties.residence, 
            properties.property_type_id,
            property_types.name AS property_type_name,
            appraisal_requests.comments,
            group_levels.levels
            FROM properties
            INNER JOIN property_types
                ON properties.property_type_id = property_types.id
            INNER JOIN appraisal_requests
                ON properties.appraisal_request_id = appraisal_requests.id
            INNER JOIN (SELECT id, property_id, max(level) AS levels FROM levels GROUP BY id, property_id) AS group_levels
                ON  group_levels.property_id = properties.id AND
                properties.property_levels = group_levels.levels
            WHERE appraisal_requests.register_type_id = 3 
            ORDER BY properties.residence
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS template_info_view");
    }
}
