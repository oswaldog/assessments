<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalAreasUniqConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_areas', function (Blueprint $table) {
            //
        });

        DB::statement('ALTER TABLE appraisal_areas ADD CONSTRAINT chk_appraisal_request_area UNIQUE (appraisal_request_id,area_id)');


    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_areas', function (Blueprint $table) {
            //
        });
    }
}
