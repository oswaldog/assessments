<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropStalesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('solicitudes', function(Blueprint $table) {
            $table->dropForeign('solicitudes_banco_id_foreign');
            $table->dropForeign('solicitudes_forma_pago_id_foreign');
            $table->dropForeign('solicitudes_status_id_foreign');
            $table->dropForeign('solicitudes_status_uso_id_foreign');
            $table->dropForeign('solicitudes_tipo_entrega_id_foreign');
            $table->dropForeign('solicitudes_tipo_inmueble_id_foreign');
        }); 

        Schema::table('inspecciones', function(Blueprint $table) {
            $table->dropForeign('inspecciones_solicitud_id_foreign');	
        });

        Schema::table('niveles', function(Blueprint $table) {
            $table->dropForeign('niveles_inspeccion_id_foreign');	
        });

        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_provincia_id_foreign');
	
        });

        
        Schema::dropIfExists('bancos');
        Schema::dropIfExists('tipo_inmueble');
        Schema::dropIfExists('status_uso');
        Schema::dropIfExists('tipo_entrega');
        Schema::dropIfExists('forma_pago');

        Schema::dropIfExists('niveles');
        Schema::dropIfExists('inspecciones');
        Schema::dropIfExists('solicitudes');
        
        Schema::dropIfExists('sectores');
        Schema::dropIfExists('ciudades');
        Schema::dropIfExists('municipios');
        Schema::dropIfExists('provincias');
        Schema::dropIfExists('paises');
 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
