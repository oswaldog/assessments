<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministrativeRolView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW administratives_roles_view AS
                            SELECT owner_user_id, member_user_id, 1 AS is_administrative 
	                        FROM work_teams
	                        WHERE (permissions -> 0 ->> 'status')::boolean = true
	                 ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS administratives_roles_view");

    }
}
