<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateTablesForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->foreign('delivery_type_id')->references('id')->on('delivery_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->foreign('bank_id')->references('id')->on('banks')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->foreign('payment_method_id')->references('id')->on('payment_methods')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->foreign('inspector_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->foreign('created_by_user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->foreign('updated_by_user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('provinces', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('countries')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('municipalities', function(Blueprint $table) {
			$table->foreign('province_id')->references('id')->on('provinces')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('cities', function(Blueprint $table) {
			$table->foreign('municipality_id')->references('id')->on('municipalities')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('sectors', function(Blueprint $table) {
			$table->foreign('city_id')->references('id')->on('cities')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('properties', function(Blueprint $table) {
			$table->foreign('appraisal_request_id')->references('id')->on('appraisal_requests')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('properties', function(Blueprint $table) {
			$table->foreign('property_type_id')->references('id')->on('property_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('properties', function(Blueprint $table) {
			$table->foreign('property_use_status_id')->references('id')->on('property_use_status')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('levels', function(Blueprint $table) {
			$table->foreign('property_id')->references('id')->on('properties')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('attachments', function(Blueprint $table) {
			$table->foreign('appraisal_request_id')->references('id')->on('appraisal_requests')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('attachments', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->dropForeign('appraisal_requests_delivery_type_id_foreign');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->dropForeign('appraisal_requests_bank_id_foreign');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->dropForeign('appraisal_requests_payment_method_id_foreign');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->dropForeign('appraisal_requests_inspector_id_foreign');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->dropForeign('appraisal_requests_created_by_user_id_foreign');
		});
		Schema::table('appraisal_requests', function(Blueprint $table) {
			$table->dropForeign('appraisal_requests_updated_by_user_id_foreign');
		});
		Schema::table('provinces', function(Blueprint $table) {
			$table->dropForeign('provinces_country_id_foreign');
		});
		Schema::table('municipalities', function(Blueprint $table) {
			$table->dropForeign('municipalities_province_id_foreign');
		});
		Schema::table('cities', function(Blueprint $table) {
			$table->dropForeign('cities_municipality_id_foreign');
		});
		Schema::table('sectors', function(Blueprint $table) {
			$table->dropForeign('sectors_city_id_foreign');
		});
		Schema::table('properties', function(Blueprint $table) {
			$table->dropForeign('properties_appraisal_request_id_foreign');
		});
		Schema::table('properties', function(Blueprint $table) {
			$table->dropForeign('properties_property_type_id_foreign');
		});
		Schema::table('properties', function(Blueprint $table) {
			$table->dropForeign('properties_property_use_status_id_foreign');
		});
		Schema::table('levels', function(Blueprint $table) {
			$table->dropForeign('levels_property_id_foreign');
		});
		Schema::table('attachments', function(Blueprint $table) {
			$table->dropForeign('attachments_appraisal_request_id_foreign');
		});
		Schema::table('attachments', function(Blueprint $table) {
			$table->dropForeign('attachments_user_id_foreign');
		});
	}
}

