<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAreasRecreateComparablesViewAddAppraisalAreasJsonFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW comparables_view AS
        SELECT 
        properties.id AS property_id,
        properties.appraisal_request_id,
        properties.property_type_id,
        property_types.name AS property_type_name,
        properties.property_use_status_id,
        property_use_status.name AS property_use_status_name,
        properties.property_number,
        properties.property_levels,
        properties.construction_year,
        properties.geo_coordinates,
        properties.cadastral_number,
        properties.registration_number,
        properties.book_number,
        properties.folio_number,
        properties.expedition_date,
        properties.is_new,
        properties.owner,
        properties.land_area,
        properties.improvement_area,
        properties.sale_date,
        properties.sale_amount,
        properties.appraisal_amount,
        properties.offer_amount,
        properties.sale_type_id,
        total_fields_view.total_rooms,
        total_fields_view.total_baths,
        total_fields_view.total_parking_spots,
        properties.residence,
        properties.address_number,
        properties.address_street,
        properties.sector_id,
        sectors.name AS sector_name,
        cities.id AS city_id,
        cities.name AS city_name,
        provinces.id AS province_id,
        provinces.name AS province_name,
        municipalities.id AS municipality_id,
        municipalities.name AS municipality_name,
        countries.id AS country_id,
        countries.name AS country_name,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'sauna'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS sauna,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'gazebo'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS gazebo,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'jacuzzi'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS jacuzzi,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'picuzzi'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS picuzzi,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'piscina'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS piscina,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'gimnasio'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS gimnasio_amenidades,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'campo_golf'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS campo_golf,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'area_juegos'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS area_juegos,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'canchas_tenis'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS canchas_tenis,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'sala_choferes'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS sala_choferes,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'salon_reuniones'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS salon_reuniones,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'terraza_techada'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS terraza_techada,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'canchas_baloncesto'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS canchas_baloncesto,
        CASE
            WHEN (((properties.general_data -> 'amenidades'::text) -> 'terraza_destechada'::text) ->> 'value'::text) = 'true'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS terraza_destechada,	
        main_photo_view.path AS photo_path,
        main_photo_view.relative_path AS photo_relative_path,
        main_photo_view.name AS photo_name,
        func_appraisal_areas_json(properties.appraisal_request_id)	
    FROM properties
        JOIN property_types ON properties.property_type_id = property_types.id
        JOIN property_use_status ON properties.property_use_status_id = property_use_status.id
        JOIN sectors ON properties.sector_id = sectors.id
        JOIN cities ON cities.id = sectors.city_id
        JOIN municipalities ON municipalities.id = cities.municipality_id
        JOIN provinces ON provinces.id = municipalities.province_id
        JOIN countries ON countries.id = provinces.country_id
        JOIN total_fields_view ON total_fields_view.property_id = properties.id
        LEFT JOIN main_photo_view ON main_photo_view.appraisal_request_id = properties.appraisal_request_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS comparables_view");
    }

}
