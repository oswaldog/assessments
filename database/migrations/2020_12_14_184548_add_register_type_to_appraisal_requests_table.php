<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRegisterTypeToAppraisalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_requests', function (Blueprint $table) {
            //
            $table->foreignId('register_type_id');
            $table->foreign('register_type_id')->references('id')->on('register_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_requests', function (Blueprint $table) {
            //
        });
    }
}
