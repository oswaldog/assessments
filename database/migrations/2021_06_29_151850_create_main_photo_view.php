<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainPhotoView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS main_photo_view");
        DB::statement("
        CREATE OR REPLACE VIEW main_photo_view AS
        WITH cte_attachments_view AS (
            SELECT
                appraisal_request_id,
                path,
                relative_path,
                name,
                pos, 
                ROW_NUMBER() OVER(PARTITION BY appraisal_request_id ORDER BY pos ASC) AS rank
            FROM attachments_view
            WHERE 
                file_type='img')
            SELECT
                appraisal_request_id,
                path,
                relative_path,
                name,
                pos
            FROM cte_attachments_view
            WHERE rank = 1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS main_photo_view");
    }
}

