<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalAttachmentsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW total_attachments_view AS
                        SELECT
                        appraisal_requests.id AS appraisal_request_id,
                        SUM(CASE WHEN attachments_view.file_type = 'img' THEN 1 ELSE 0 END) AS total_img,
                        SUM(CASE WHEN attachments_view.file_type = 'doc' THEN 1 ELSE 0 END) AS total_doc
                        FROM appraisal_requests
                        LEFT JOIN attachments_view
                            ON attachments_view.appraisal_request_id = appraisal_requests.id
                        GROUP BY 1
	                 ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS total_attachments_view");
    }
}
