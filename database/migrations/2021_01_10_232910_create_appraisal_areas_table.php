<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_areas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('appraisal_request_id');
            $table->unsignedBigInteger('area_id');
            $table->integer('dimension');
            $table->foreign('appraisal_request_id')->references('id')->on('appraisal_requests');
            $table->foreign('area_id')->references('id')->on('areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_areas');
    }
}
