<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeApplicantAndContactNameSizeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS inspections_view");
        DB::statement("DROP VIEW IF EXISTS template_info_view");
        DB::statement('DROP VIEW IF EXISTS appraisal_requests_rpt_view');
        DB::statement("DROP VIEW IF EXISTS appraisal_requests_view");


        Schema::table('appraisal_requests', function (Blueprint $table) {
            //
			$table->string('applicant_name', 100)->change();
			$table->string('contact_name', 100)->change();
            $table->string('applicant_email', 50)->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_requests', function (Blueprint $table) {
            //
        });
    }
}
