<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSolicitudesTable extends Migration {

	public function up()
	{
		Schema::create('solicitudes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 30);
			$table->string('apellido', 30);
			$table->string('telefono', 30);
			$table->string('email', 50);
			$table->integer('banco_id');
			$table->date('fecha_inspeccion');
			$table->time('hora_inspeccion');
			$table->date('fecha_entrega');
			$table->time('hora_entrega');			
			$table->integer('tipo_inmueble_id');
			$table->integer('status_uso_id');
			$table->string('nro_inmueble', 30);
			$table->string('latitud', 20);
			$table->string('longitud', 20);
			$table->string('direccion', 100);
			$table->integer('municipio_id')->unsigned();
			$table->integer('created_by_id');
			$table->integer('updated_by_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('solicitudes');
	}
}