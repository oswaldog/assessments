<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActiveRolesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        DB::statement("CREATE OR REPLACE VIEW member_users_roles_view AS
                        SELECT 
                        owner_user_id,
                        member_user_id, 
                        unnest(array_values)::INTEGER AS role
                        FROM active_permissions_view
                    ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS member_users_roles_view");

    }
}