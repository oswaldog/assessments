<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAreasRecreateAppraisalRequestReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        DB::statement("CREATE OR REPLACE VIEW appraisal_requests_rpt_view AS
            SELECT
                properties.appraisal_request_id,
                appraisal_requests.applicant_name,
                appraisal_requests.applicant_phone,
                appraisal_requests.applicant_email,
                appraisal_requests.contact_name,
                appraisal_requests.contact_phone,
                delivery_types.name AS delivery_type_name,
                banks.name AS bank_name,
                appraisal_requests.amount,
                payment_methods.name AS payment_method_name,
                CASE WHEN appraisal_requests.is_paid = true THEN 'Pagada' ELSE 'Pendiente' END AS payment_status,
                appraisal_requests.inspection_date,
                appraisal_requests.inspection_time,
                appraisal_requests.delivery_date,
                users.name AS inspector_name,
                appraisal_status.name AS appraisal_status_name,
                register_types.name AS register_type_name,
                appraisal_requests.address,
                appraisal_requests.comments,
                property_types.name AS property_type_name,
                property_use_status.name AS property_use_status_name,
                properties.property_number,
                properties.property_levels,
                properties.construction_year,
                properties.geo_coordinates,
                properties.cadastral_number,
                properties.registration_number,
                properties.book_number,
                properties.folio_number,
                properties.expedition_date,
                CASE WHEN properties.is_new = true THEN 'Nuevo' ELSE 'Usado' END AS property_status,
                properties.owner,
                properties.land_area,
                properties.improvement_area,
                properties.residence,
                properties.address_number,
                properties.address_street,
                sectors.name AS sector_name,
                cities.name AS city_name,
                municipalities.name AS municipality_name,
                provinces.name AS province_name, 
                countries.name AS country_name
            FROM appraisal_requests
                JOIN properties ON appraisal_requests.id = properties.appraisal_request_id
                JOIN delivery_types ON appraisal_requests.delivery_type_id = delivery_types.id
                JOIN banks ON appraisal_requests.bank_id = banks.id
                JOIN payment_methods ON appraisal_requests.payment_method_id = payment_methods.id
                JOIN users ON appraisal_requests.inspector_id = users.id
                JOIN appraisal_status ON appraisal_requests.appraisal_status_id = appraisal_status.id
                JOIN register_types ON appraisal_requests.register_type_id = register_types.id
                JOIN property_types ON properties.property_type_id = property_types.id
                JOIN property_use_status ON properties.property_use_status_id = property_use_status.id
                JOIN sectors ON properties.sector_id = sectors.id
                JOIN cities ON cities.id = sectors.city_id
                JOIN municipalities ON municipalities.id = cities.municipality_id
                JOIN provinces ON provinces.id = municipalities.province_id
                JOIN countries ON countries.id = provinces.country_id
            ORDER BY appraisal_requests.inspection_date, appraisal_requests.inspection_time;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DROP VIEW IF EXISTS appraisal_requests_rpt_view');
    }


}
