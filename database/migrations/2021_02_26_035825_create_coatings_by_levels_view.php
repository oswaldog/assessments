<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoatingsByLevelsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW coatings_by_levels_view AS
                        SELECT
                            appraisal_request_id,
                            property_id,
                            level,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'ventanas'->'madera'->>'value' = 'true' THEN internal_data->'revestimientos'->'ventanas'->'madera'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'ventanas'->'selocias'->>'value' = 'true' THEN internal_data->'revestimientos'->'ventanas'->'selocias'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'ventanas'->'batientes'->>'value' = 'true' THEN internal_data->'revestimientos'->'ventanas'->'batientes'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'ventanas'->'madera_cristal'->>'value' = 'true' THEN internal_data->'revestimientos'->'ventanas'->'madera_cristal'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'ventanas'->'corredizas_aluminio_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'ventanas'->'corredizas_aluminio_vidrio'->>'text' END			 
                            ) AS ventanas,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'vinil'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'vinil'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'marmol'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'marmol'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'piedra'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'piedra'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'quarzo'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'quarzo'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'granito'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'granito'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'parquet'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'parquet'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'ceramica'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'ceramica'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'coralina'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'coralina'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'mosaicos'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'mosaicos'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'marmolite'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'marmolite'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'porcelanato'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'porcelanato'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'piso_flotante'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'piso_flotante'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'cemento_pulido'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'cemento_pulido'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos'->'estado_primario'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos'->'estado_primario'->>'text' END
                            ) AS pisos,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'vinil'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'vinil'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'marmol'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'marmol'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'piedra'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'piedra'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'quarzo'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'quarzo'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'granito'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'granito'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'parquet'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'parquet'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'ceramica'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'ceramica'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'coralina'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'coralina'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'mosaicos'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'mosaicos'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'marmolite'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'marmolite'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'porcelanato'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'porcelanato'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'piso_flotante'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'piso_flotante'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'cemento_pulido'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'cemento_pulido'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_cocina'->'estado_primario'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_cocina'->'estado_primario'->>'text' END			 
                            ) AS pared_cocina,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'vinil'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'vinil'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'marmol'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'marmol'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'piedra'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'piedra'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'quarzo'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'quarzo'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'granito'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'granito'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'parquet'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'parquet'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'ceramica'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'ceramica'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'coralina'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'coralina'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'mosaicos'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'mosaicos'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'marmolite'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'marmolite'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'porcelanato'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'porcelanato'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'piso_flotante'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'piso_flotante'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'cemento_pulido'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'cemento_pulido'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'topes_cocina'->'estado_primario'->>'value' = 'true' THEN internal_data->'revestimientos'->'topes_cocina'->'estado_primario'->>'text' END
                            ) AS topes_cocina,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'vinil'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'vinil'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'marmol'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'marmol'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'piedra'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'piedra'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'quarzo'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'quarzo'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'granito'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'granito'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'parquet'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'parquet'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'ceramica'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'ceramica'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'coralina'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'coralina'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'mosaicos'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'mosaicos'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'marmolite'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'marmolite'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'porcelanato'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'porcelanato'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'piso_flotante'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'piso_flotante'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'cemento_pulido'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'cemento_pulido'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pared_sanitarios'->'estado_primario'->>'value' = 'true' THEN internal_data->'revestimientos'->'pared_sanitarios'->'estado_primario'->>'text' END	
                            ) AS pared_sanitarios,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'vinil'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'vinil'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'marmol'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'marmol'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'piedra'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'piedra'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'quarzo'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'quarzo'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'granito'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'granito'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'parquet'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'parquet'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'ceramica'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'ceramica'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'coralina'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'coralina'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'mosaicos'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'mosaicos'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'marmolite'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'marmolite'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'porcelanato'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'porcelanato'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'piso_flotante'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'piso_flotante'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'cemento_pulido'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'cemento_pulido'->>'text' END,
                            CASE WHEN internal_data->'revestimientos'->'pisos_sanitarios'->'estado_primario'->>'value' = 'true' THEN internal_data->'revestimientos'->'pisos_sanitarios'->'estado_primario'->>'text' END
                            ) AS pisos_sanitarios,
                                concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'techos'->'madera'->>'value' = 'true' THEN internal_data->'revestimientos'->'techos'->'madera'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techos'->'plafond'->>'value' = 'true' THEN internal_data->'revestimientos'->'techos'->'plafond'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techos'->'sheetrock'->>'value' = 'true' THEN internal_data->'revestimientos'->'techos'->'sheetrock'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techos'->'cornisas_yeso'->>'value' = 'true' THEN internal_data->'revestimientos'->'techos'->'cornisas_yeso'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techos'->'panete_pintura'->>'value' = 'true' THEN internal_data->'revestimientos'->'techos'->'panete_pintura'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techos'->'losa_estado_primario'->>'value' = 'true' THEN internal_data->'revestimientos'->'techos'->'losa_estado_primario'->>'text' END 			 
                            ) AS techos,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'techo_sanitario'->'madera'->>'value' = 'true' THEN internal_data->'revestimientos'->'techo_sanitario'->'madera'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techo_sanitario'->'plafond'->>'value' = 'true' THEN internal_data->'revestimientos'->'techo_sanitario'->'plafond'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techo_sanitario'->'sheetrock'->>'value' = 'true' THEN internal_data->'revestimientos'->'techo_sanitario'->'sheetrock'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techo_sanitario'->'cornisas_yeso'->>'value' = 'true' THEN internal_data->'revestimientos'->'techo_sanitario'->'cornisas_yeso'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techo_sanitario'->'panete_pintura'->>'value' = 'true' THEN internal_data->'revestimientos'->'techo_sanitario'->'panete_pintura'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'techo_sanitario'->'losa_estado_primario'->>'value' = 'true' THEN internal_data->'revestimientos'->'techo_sanitario'->'losa_estado_primario'->>'text' END 			 
                            ) AS techo_sanitario,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'closets'->'pino'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'pino'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'caoba'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'caoba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'cedro'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'cedro'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'roble'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'roble'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'plywood'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'plywood'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'andiroba'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'andiroba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'jaquitiba'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'jaquitiba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'sin_puertas'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'sin_puertas'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'madera_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'madera_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'aluminio_espejo'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'aluminio_espejo'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'aluminio_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'aluminio_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'madera_preciosa'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'madera_preciosa'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'closets'->'madera_prensada'->>'value' = 'true' THEN internal_data->'revestimientos'->'closets'->'madera_prensada'->>'text' END 
                            ) AS closets,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'pino'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'pino'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'caoba'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'caoba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'cedro'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'cedro'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'roble'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'roble'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'plywood'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'plywood'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'andiroba'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'andiroba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'jaquitiba'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'jaquitiba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'sin_puertas'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'sin_puertas'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'madera_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'madera_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'aluminio_espejo'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'aluminio_espejo'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'aluminio_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'aluminio_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'madera_preciosa'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'madera_preciosa'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'jambas_puertas'->'madera_prensada'->>'value' = 'true' THEN internal_data->'revestimientos'->'jambas_puertas'->'madera_prensada'->>'text' END 
                            ) AS jambas_puertas,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'pino'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'pino'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'caoba'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'caoba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'cedro'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'cedro'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'roble'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'roble'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'plywood'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'plywood'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'andiroba'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'andiroba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'jaquitiba'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'jaquitiba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'sin_puertas'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'sin_puertas'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'madera_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'madera_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'aluminio_espejo'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'aluminio_espejo'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'aluminio_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'aluminio_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'madera_preciosa'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'madera_preciosa'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'walking_closets'->'madera_prensada'->>'value' = 'true' THEN internal_data->'revestimientos'->'walking_closets'->'madera_prensada'->>'text' END 
                            ) AS walking_closets,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'pino'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'pino'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'caoba'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'caoba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'cedro'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'cedro'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'roble'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'roble'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'plywood'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'plywood'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'andiroba'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'andiroba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'jaquitiba'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'jaquitiba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'sin_puertas'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'sin_puertas'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'madera_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'madera_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'aluminio_espejo'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'aluminio_espejo'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'aluminio_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'aluminio_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'madera_preciosa'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'madera_preciosa'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'gabinetes_cocina'->'madera_prensada'->>'value' = 'true' THEN internal_data->'revestimientos'->'gabinetes_cocina'->'madera_prensada'->>'text' END 
                            ) AS gabinetes_cocina,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'pino'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'pino'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'caoba'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'caoba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'cedro'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'cedro'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'roble'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'roble'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'plywood'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'plywood'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'andiroba'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'andiroba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'jaquitiba'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'jaquitiba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'sin_puertas'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'sin_puertas'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'madera_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'madera_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'aluminio_espejo'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'aluminio_espejo'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'aluminio_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'aluminio_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'madera_preciosa'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'madera_preciosa'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puerta_principal'->'madera_prensada'->>'value' = 'true' THEN internal_data->'revestimientos'->'puerta_principal'->'madera_prensada'->>'text' END 
                            ) AS puerta_principal,
                                concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'pino'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'pino'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'caoba'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'caoba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'cedro'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'cedro'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'roble'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'roble'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'plywood'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'plywood'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'andiroba'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'andiroba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'jaquitiba'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'jaquitiba'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'sin_puertas'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'sin_puertas'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'madera_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'madera_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'aluminio_espejo'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'aluminio_espejo'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'aluminio_vidrio'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'aluminio_vidrio'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'madera_preciosa'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'madera_preciosa'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'puertas_interiores'->'madera_prensada'->>'value' = 'true' THEN internal_data->'revestimientos'->'puertas_interiores'->'madera_prensada'->>'text' END 
                            ) AS puertas_interiores,
                            concat_ws(', ',
                            CASE WHEN internal_data->'revestimientos'->'ducha_sanitario'->'tina'->>'value' = 'true' THEN internal_data->'revestimientos'->'ducha_sanitario'->'tina'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'ducha_sanitario'->'cabina'->>'value' = 'true' THEN internal_data->'revestimientos'->'ducha_sanitario'->'cabina'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'ducha_sanitario'->'mampara'->>'value' = 'true' THEN internal_data->'revestimientos'->'ducha_sanitario'->'mampara'->>'text' END, 
                            CASE WHEN internal_data->'revestimientos'->'ducha_sanitario'->'puertas_corredizas'->>'value' = 'true' THEN internal_data->'revestimientos'->'ducha_sanitario'->'puertas_corredizas'->>'text' END 
                            ) AS ducha_sanitario
                        FROM levels
                        INNER JOIN  properties
                            ON levels.property_id = properties.id
                        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS coatings_by_levels_view");
    }
}
