<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsTableSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->string('nombre_contacto',50)->comment('Nombre de la persona contacto');
            $table->string('telefono_contacto',30)->comment('Telefono de la persona contacto');
            $table->decimal('monto',10,2)->comment('Monto a cobrar');
            $table->boolean('pagado')->comment('Estatus del pago de la tasacion');
            $table->string('comentarios',500)->nullable()->comment('Comentarios adicionales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
