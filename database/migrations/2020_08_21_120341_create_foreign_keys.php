<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('solicitudes', function(Blueprint $table) {
			$table->foreign('banco_id')->references('id')->on('bancos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('solicitudes', function(Blueprint $table) {
			$table->foreign('tipo_inmueble_id')->references('id')->on('tipo_inmueble')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('solicitudes', function(Blueprint $table) {
			$table->foreign('status_uso_id')->references('id')->on('status_uso')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('inspecciones', function(Blueprint $table) {
			$table->foreign('solicitud_id')->references('id')->on('solicitudes')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('niveles', function(Blueprint $table) {
			$table->foreign('inspeccion_id')->references('id')->on('inspecciones')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{ /*
		Schema::table('solicitudes', function(Blueprint $table) {
			$table->dropForeign('solicitudes_municipio_id_foreign');
		});
		Schema::table('inspecciones', function(Blueprint $table) {
			$table->dropForeign('inspecciones_solicitudes_id_foreign');
		});
		Schema::table('provincias', function(Blueprint $table) {
			$table->dropForeign('provincias_region_id_foreign');
		});
		Schema::table('municipios', function(Blueprint $table) {
			$table->dropForeign('municipios_provincia_id_foreign');
		});
		Schema::table('niveles', function(Blueprint $table) {
			$table->dropForeign('niveles_inspecciones_id_foreign');
		});
		*/
	}
}