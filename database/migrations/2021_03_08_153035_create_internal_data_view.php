<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalDataView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW internal_data_view AS
                        SELECT
                            appraisal_request_id,
                            property_id,
                            level,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'recibidor' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS recibidor,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'sala' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS sala,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'comedor' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS comedor,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'cocina' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS cocina,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'cocina_fria_caliente' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS cocina_fria_caliente,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'cocina_desayunador' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS cocina_desayunador,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'cocina_isla' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS cocina_isla,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'area_lavado' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS area_lavado,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'cuarto_servicio' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS cuarto_servicio,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'sanitario_servicio' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS sanitario_servicio,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'terraza' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS terraza,
                            CASE WHEN levels.internal_data -> 'distribucion_interna' -> 'patio' ->> 'value' = 'true' THEN 'SI' ELSE 'NO' END AS patio,                            
                            levels.internal_data -> 'distribucion_interna' -> 'medio_sanitario' ->> 'value' AS medio_sanitario,
                            levels.internal_data -> 'distribucion_interna' -> 'dormitorios_grandes' ->> 'value' AS dormitorios_grandes,
                            levels.internal_data -> 'distribucion_interna' -> 'dormitorios_medianos' ->> 'value' AS dormitorios_medianos,
                            levels.internal_data -> 'distribucion_interna' -> 'dormitorios_pequenos' ->> 'value' AS dormitorios_pequenos,
                            levels.internal_data -> 'distribucion_interna' -> 'sanitarios' ->> 'value' AS sanitarios,
                            levels.internal_data -> 'distribucion_interna' -> 'closets' ->> 'value' AS closets,
                            levels.internal_data -> 'distribucion_interna' -> 'walking_closets' ->> 'value' AS walking_closets,
                            levels.internal_data -> 'distribucion_interna' -> 'closet_desahogo' ->> 'value' AS closet_desahogo,
                            levels.internal_data -> 'distribucion_interna' -> 'sala_estar' ->> 'value' AS sala_estar,
                            levels.internal_data -> 'distribucion_interna' -> 'balcon' ->> 'value' AS balcon 
                        FROM levels
                        INNER JOIN  properties
                            ON levels.property_id = properties.id
                        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS internal_data_view");
    }
}
