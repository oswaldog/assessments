<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspeccionesTable extends Migration {

	public function up()
	{
		Schema::create('inspecciones', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('solicitud_id')->unsigned();
			$table->integer('niveles');
			$table->json('data');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('inspecciones');
	}
}