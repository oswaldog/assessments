<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplateInfoView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS template_info_view");
        DB::statement("
        CREATE OR REPLACE VIEW template_info_view AS
            SELECT 
            appraisal_requests_view.id AS template_id,
            appraisal_requests_view.property_id,
            residence,
            comments,
            property_type_name,
            property_type_id,
            group_levels.levels
            FROM 
                appraisal_requests_view
            INNER JOIN 
                (SELECT id, property_id, max(level) AS levels FROM levels GROUP BY id, property_id) AS group_levels
                ON  group_levels.property_id = appraisal_requests_view.property_id AND
                appraisal_requests_view.property_levels = group_levels.levels
            WHERE 
                register_type_id = 3 
            ORDER BY residence
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS template_info_view");
    }
}
