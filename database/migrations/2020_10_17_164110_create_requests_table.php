<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('appraisal_requests', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('applicant_name', 50);
			$table->string('applicant_phone', 30);
			$table->string('applicant_email', 50);
			$table->string('contact_name', 50);
			$table->string('contact_phone', 30);
			$table->integer('delivery_type_id')->unsigned();
			$table->integer('bank_id')->unsigned();
			$table->decimal('amount', 10,2)->default('0');
			$table->integer('payment_method_id')->unsigned();
			$table->boolean('is_paid')->default(false);
			$table->date('inspection_date');
			$table->time('inspection_time');
			$table->date('delivery_date');
			$table->bigInteger('inspector_id')->unsigned()->nullable()->index();
			$table->string('address', 500);
			$table->string('comments', 500)->nullable();
			$table->bigInteger('created_by_user_id')->unsigned();
			$table->bigInteger('updated_by_user_id')->unsigned()->nullable();
		});
	}


    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		Schema::drop('appraisal_requests');
	}
}
