<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('properties', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->bigInteger('appraisal_request_id')->unsigned();
			$table->integer('property_type_id')->unsigned()->index();
			$table->integer('property_use_status_id')->unsigned();
			$table->string('property_number', 50);
			$table->smallInteger('property_levels')->unsigned()->default('1');
			$table->smallInteger('construction_year')->unsigned();
			$table->string('geo_coordinates', 100);
			$table->string('cadastral_number', 100)->nullable();
			$table->string('registration_number', 100);
			$table->string('book_number', 100);
			$table->string('folio_number', 100);
			$table->date('expedition_date');
			$table->boolean('is_new')->default(false);
			$table->string('owner', 150);
			$table->integer('land_area')->default('0');
			$table->integer('improvement_area')->default('0');
			$table->integer('sector_id')->unsigned()->index();
			$table->string('residence',100)->nullable();
			$table->string('address_number', 50);
			$table->string('address_street', 100);
			$table->jsonb('general_data')->nullable();
			$table->date('sale_date')->nullable();
			$table->decimal('sale_amount',10,2)->nullable();			
			$table->decimal('appraisal_amount',10,2)->nullable();
			$table->decimal('offer_amount',10,2)->nullable();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		Schema::drop('properties');
	}
}
