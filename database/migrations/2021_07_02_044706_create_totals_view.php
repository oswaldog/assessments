<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     public function up()
    {
        DB::statement("DROP VIEW IF EXISTS total_rooms_baths_view");
        DB::statement("DROP VIEW IF EXISTS total_fields_view");
        DB::statement("
        CREATE OR REPLACE VIEW public.total_fields_view
        AS
        SELECT internal_data_view.appraisal_request_id,
           internal_data_view.property_id,
           SUM(internal_data_view.dormitorios_grandes::INTEGER + internal_data_view.dormitorios_medianos::INTEGER + internal_data_view.dormitorios_pequenos::INTEGER) AS total_rooms,
           SUM(internal_data_view.sanitarios::INTEGER + internal_data_view.medio_sanitario::INTEGER) AS total_baths,
           COALESCE(((properties.general_data -> 'informacion_general'::text) -> 'parqueos_aire_libre'::text) ->> 'value'::text, '0'::text)::INTEGER +
           COALESCE(((properties.general_data -> 'informacion_general'::text) -> 'parqueos_techados'::text) ->> 'value'::text, '0'::text)::INTEGER AS total_parking_spots
          FROM internal_data_view
          JOIN properties ON internal_data_view.property_id = properties.id
         GROUP BY 1,2,5");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS total_rooms_baths_view");
        DB::statement("DROP VIEW IF EXISTS total_fields_view");
    }

}
