<?php

use Illuminate\Database\Seeder;

class BanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('banks')->insert([
            ['id' => 0, 'name' => 'N/A'],
            ['id' => 1, 'name' => 'Bancamerica'],
            ['id' => 2, 'name' => 'Banco Ademi'],
            ['id' => 3, 'name' => 'Banco Adopem'],
            ['id' => 4, 'name' => 'Banco Agrícola de la República Dominicana'],
            ['id' => 5, 'name' => 'Banco Atlántico'],
            ['id' => 6, 'name' => 'Banco Atlas'],
            ['id' => 7, 'name' => 'Banco Bancotui'],
            ['id' => 8, 'name' => 'Banco BDA'],
            ['id' => 9, 'name' => 'Banco BDI'],
            ['id' => 10, 'name' => 'Banco BELLBANK'],
            ['id' => 11, 'name' => 'Banco BHD León'],
            ['id' => 12, 'name' => 'Banco Bonanza'],
            ['id' => 13, 'name' => 'Banco Capital'],
            ['id' => 14, 'name' => 'Banco Caribe'],
            ['id' => 15, 'name' => 'Banco Cofaci'],
            ['id' => 16, 'name' => 'Banco Confisa'],
            ['id' => 17, 'name' => 'Banco Del Caribe'],
            ['id' => 18, 'name' => 'Banco del Progreso'],
            ['id' => 19, 'name' => 'Banco Empire'],
            ['id' => 20, 'name' => 'Banco Federal'],
            ['id' => 21, 'name' => 'Banco Fihogar'],
            ['id' => 22, 'name' => 'Banco Gruficorp'],
            ['id' => 23, 'name' => 'Banco Inmobiliario (Banaci)'],
            ['id' => 24, 'name' => 'Banco López de H'],
            ['id' => 25, 'name' => 'Banco Micro'],
            ['id' => 26, 'name' => 'Banco Motor Crédito'],
            ['id' => 27, 'name' => 'Banco Múltiple Activo Dominicana'],
            ['id' => 28, 'name' => 'Banco Popular Dominicano'],
            ['id' => 29, 'name' => 'Banco Promerica'],
            ['id' => 30, 'name' => 'Banco Providencial'],
            ['id' => 31, 'name' => 'Banco Pyme Bhd'],
            ['id' => 32, 'name' => 'Banco Rio'],
            ['id' => 33, 'name' => 'Banco Santa Cruz'],
            ['id' => 34, 'name' => 'Banco Union'],
            ['id' => 35, 'name' => 'Banesco'],
            ['id' => 36, 'name' => 'Citibank'],
            ['id' => 37, 'name' => 'Scotiabank'],
        ]);
    }
}
