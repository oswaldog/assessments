<?php

use Illuminate\Database\Seeder;

class CoatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('coatings')->insert([
            ['id' => 1, 'name' => 'construcción', 'description' => 'Pisos, Pared de Cocina, Tope de cocina, pared de baño,  piso de baño'],
            ['id' => 2, 'name' => 'ventanas', 'description' => 'Ventanas'],
            ['id' => 3, 'name' => 'techos', 'description' => 'Techos, Techos de baño'],
            ['id' => 4, 'name' => 'madereros', 'description' => 'Puerta Principal, Puertas interiores, Jambas, closet, walking closet, gabinetes de cocina'],
            ['id' => 5, 'name' => 'sanitarios', 'description' => 'Ducha de Baño'],       
        ]);
    }
}
