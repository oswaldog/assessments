<?php

use Illuminate\Database\Seeder;

class SaleTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sale_types')->insert([
            ['id' => 1, 'name' => 'Venta Directa'],
            ['id' => 2, 'name' => 'Intermediario'],
            ]); 
    }
}
