<?php

use Illuminate\Database\Seeder;

class CoatingMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('coating_materials')->insert([
            ['id' => 1, 'coating_id' => 1, 'text_value' => 'Cerámica', 'name' => 'ceramica'],
            ['id' => 2, 'coating_id' => 1, 'text_value' => 'Porcelanato', 'name' => 'porcelanato'],
            ['id' => 3, 'coating_id' => 1, 'text_value' => 'Granito', 'name' => 'granito'],
            ['id' => 4, 'coating_id' => 1, 'text_value' => 'Mármol', 'name' => 'marmol'],
            ['id' => 5, 'coating_id' => 1, 'text_value' => 'Parquet', 'name' => 'parquet'],
            ['id' => 6, 'coating_id' => 1, 'text_value' => 'Cemento Pulido', 'name' => 'cemento_pulido'],
            ['id' => 7, 'coating_id' => 1, 'text_value' => 'Mosaicos', 'name' => 'mosaicos'],
            ['id' => 8, 'coating_id' => 1, 'text_value' => 'Quarzo', 'name' => 'quarzo'],
            ['id' => 9, 'coating_id' => 1, 'text_value' => 'Marmolite', 'name' => 'marmolite'],
            ['id' => 10, 'coating_id' => 1, 'text_value' => 'Piso Flotante', 'name' => 'piso_flotante'],
            ['id' => 11, 'coating_id' => 1, 'text_value' => 'Vinil', 'name' => 'vinil'],
            ['id' => 12, 'coating_id' => 1, 'text_value' => 'Coralina', 'name' => 'coralina'],
            ['id' => 13, 'coating_id' => 1, 'text_value' => 'Piedra', 'name' => 'piedra'],
            ['id' => 14, 'coating_id' => 1, 'text_value' => 'Estado primario', 'name' => 'estado_primario'],
            ['id' => 15, 'coating_id' => 2, 'text_value' => 'Corredizas de aluminio y vidrio ', 'name' => 'corredizas_aluminio_vidrio'],
            ['id' => 16, 'coating_id' => 2, 'text_value' => 'Selocias', 'name' => 'selocias'],
            ['id' => 17, 'coating_id' => 2, 'text_value' => 'Madera', 'name' => 'madera'],
            ['id' => 18, 'coating_id' => 2, 'text_value' => 'Batientes', 'name' => 'batientes'],
            ['id' => 19, 'coating_id' => 2, 'text_value' => 'Madera y cristal', 'name' => 'madera_cristal'],
            ['id' => 20, 'coating_id' => 3, 'text_value' => 'Pañete y Pintura', 'name' => 'panete_pintura'],
            ['id' => 21, 'coating_id' => 3, 'text_value' => 'Cornisas de yeso', 'name' => 'cornisas_yeso'],
            ['id' => 22, 'coating_id' => 3, 'text_value' => 'Sheetrock', 'name' => 'sheetrock'],
            ['id' => 23, 'coating_id' => 3, 'text_value' => 'Madera', 'name' => 'madera'],
            ['id' => 24, 'coating_id' => 3, 'text_value' => 'Plafond', 'name' => 'plafond'],
            ['id' => 25, 'coating_id' => 3, 'text_value' => 'losa en estado primario ', 'name' => 'losa_estado_primario'],
            ['id' => 26, 'coating_id' => 4, 'text_value' => 'Caoba', 'name' => 'caoba'],
            ['id' => 27, 'coating_id' => 4, 'text_value' => 'Roble', 'name' => 'roble'],
            ['id' => 28, 'coating_id' => 4, 'text_value' => 'Cedro', 'name' => 'cedro'],
            ['id' => 29, 'coating_id' => 4, 'text_value' => 'Pino', 'name' => 'pino'],
            ['id' => 30, 'coating_id' => 4, 'text_value' => 'Andiroba', 'name' => 'andiroba'],
            ['id' => 31, 'coating_id' => 4, 'text_value' => 'Madera Prensada', 'name' => 'madera_prensada'],
            ['id' => 32, 'coating_id' => 4, 'text_value' => 'Jaquitiba', 'name' => 'jaquitiba'],
            ['id' => 33, 'coating_id' => 4, 'text_value' => 'Plywood', 'name' => 'plywood'],
            ['id' => 34, 'coating_id' => 4, 'text_value' => 'Madera Preciosa', 'name' => 'madera_preciosa'],
            ['id' => 35, 'coating_id' => 4, 'text_value' => 'Madera y Vidrio', 'name' => 'madera_vidrio'],
            ['id' => 36, 'coating_id' => 4, 'text_value' => 'Aluminio y vidrio', 'name' => 'aluminio_vidrio'],
            ['id' => 37, 'coating_id' => 4, 'text_value' => 'Aluminio y espejo', 'name' => 'aluminio_espejo'],
            ['id' => 38, 'coating_id' => 4, 'text_value' => 'sin puertas', 'name' => 'sin_puertas'],
            ['id' => 39, 'coating_id' => 5, 'text_value' => 'Bañera', 'name' => 'tina'],
            ['id' => 40, 'coating_id' => 5, 'text_value' => 'Puertas corredizas', 'name' => 'puertas_corredizas'],
            ['id' => 41, 'coating_id' => 5, 'text_value' => 'Mampara', 'name' => 'mampara'],
            ['id' => 42, 'coating_id' => 5, 'text_value' => 'Cabina', 'name' => 'cabina'],
        ]);
    }
}
