<?php

use Illuminate\Database\Seeder;

class PropertyUseStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('property_use_status')->insert([
            ['id' => 1, 'name' => 'Desocupada'],
            ['id' => 2, 'name' => 'Inquilino'],
            ['id' => 3, 'name' => 'Propietario'],
        ]);
    }
}
