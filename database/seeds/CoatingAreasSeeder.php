<?php

use Illuminate\Database\Seeder;

class CoatingAreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */





    public function run()
    {
        DB::table('coating_areas')->insert([
            ['id' => 1, 'coating_id' => 1, 'text_value' => 'Pisos', 'name' => 'pisos'],
            ['id' => 2, 'coating_id' => 3, 'text_value' => 'Techos', 'name' => 'techos'],
            ['id' => 3, 'coating_id' => 4, 'text_value' => 'Closets', 'name' => 'closets'],
            ['id' => 4, 'coating_id' => 2, 'text_value' => 'Ventanas', 'name' => 'ventanas'],
            ['id' => 5, 'coating_id' => 1, 'text_value' => 'Pared de Cocina', 'name' => 'pared_cocina'],
            ['id' => 6, 'coating_id' => 1, 'text_value' => 'Topes de Cocina', 'name' => 'topes_cocina'],
            ['id' => 7, 'coating_id' => 4, 'text_value' => 'Jambas en Puertas', 'name' => 'jambas_puertas'],
            ['id' => 8, 'coating_id' => 5, 'text_value' => 'Ducha de Baño', 'name' => 'ducha_sanitario'],
            ['id' => 9, 'coating_id' => 3, 'text_value' => 'Techo de Baño', 'name' => 'techo_sanitario'],
            ['id' => 10, 'coating_id' => 4, 'text_value' => 'Walking Closet', 'name' => 'walking_closets'],
            ['id' => 11, 'coating_id' => 4, 'text_value' => 'Gabinetes de Cocina', 'name' => 'gabinetes_cocina'],
            ['id' => 12, 'coating_id' => 1, 'text_value' => 'Pared de Baño', 'name' => 'pared_sanitarios'],
            ['id' => 13, 'coating_id' => 1, 'text_value' => 'Piso de Baño', 'name' => 'pisos_sanitarios'],
            ['id' => 14, 'coating_id' => 4, 'text_value' => 'Puerta Principal', 'name' => 'puerta_principal'],
            ['id' => 15, 'coating_id' => 4, 'text_value' => 'Puertas Interiores', 'name' => 'puertas_interiores'],
        ]);    
    }
}
