<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            ['id' => 1, 'name' => 'Administrativo'],
            ['id' => 2, 'name' => 'Inspector'],
            ['id' => 3, 'name' => 'Evaluador'],
            ['id' => 4, 'name' => 'Aprobador'],
        ]);
    }
}
