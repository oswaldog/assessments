<?php

use Illuminate\Database\Seeder;

class AppraisalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appraisal_status')->insert([
            ['id' => 1, 'name' => 'Agendada', 'description' => 'Solicitud asignada en espera de ser inspeccionada'],
            ['id' => 2, 'name' => 'Inspeccionada', 'description' => 'Solicitud inspeccionada en espera de ser revisada'],
            ['id' => 3, 'name' => 'Revisada', 'description' => 'Solicitud revisada en espera de ser evaluada'],
            ['id' => 4, 'name' => 'Evaluada', 'description' => 'Solicitud evaluada en espera de ser aprobada'],
            ['id' => 5, 'name' => 'Aprobada', 'description' => 'Solicitud aprobada. Proceso completado'],
            ['id' => 6, 'name' => 'Cerrada', 'description' => 'Solicitud no completada y cerrada'],
            ]);   
    }
}
