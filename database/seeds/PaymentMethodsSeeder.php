<?php

use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('payment_methods')->insert([
            ['id' => 1, 'name' => 'Cheque'],
            ['id' => 2, 'name' => 'Efectivo'],
            ['id' => 3, 'name' => 'Transferencia/Depósito'],
        ]);
    }
}
