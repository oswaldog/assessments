<?php

use Illuminate\Database\Seeder;

class ProvincesSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('provinces')->insert([
            ['id' => 1, 'name' => 'Distrito Nacional', 'country_id' => 1],
            ['id' => 2, 'name' => 'Puerto Plata', 'country_id' => 1],
 
        ]);
    }
}
