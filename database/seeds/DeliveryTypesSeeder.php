<?php

use Illuminate\Database\Seeder;

class DeliveryTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('delivery_types')->insert([
            ['id' => 1, 'name' => 'Banco'],
            ['id' => 2, 'name' => 'Personal'],
            ['id' => 3, 'name' => 'Ambas'],
        ]);    
    }
}
