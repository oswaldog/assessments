<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            
            BanksSeeder::class,
            CountriesSeeders::class,
            ProvincesSeeders::class,
            PropertyUseStatusSeeder::class,
            PropertyTypesSeeder::class,
            PaymentMethodsSeeder::class,
            DeliveryTypesSeeder::class,
            AppraisalStatusSeeder::class,
            RolesSeeder::class,
            CoatingSeeder::class,
            CoatingMaterialSeeder::class,
            CoatingAreasSeeder::class,
            RegisterTypesSeeder::class,
            SaleTypesSeeder::class,
        ]);
    }
}
