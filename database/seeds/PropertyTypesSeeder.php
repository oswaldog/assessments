<?php

use Illuminate\Database\Seeder;

class PropertyTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('property_types')->insert([                                          
            ['id' => 1, 'name' => 'Apartamento'],
            ['id' => 2, 'name' => 'Casa'],
            ['id' => 3, 'name' => 'Solar'],
        ]);
    }
}
