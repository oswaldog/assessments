<?php

use Illuminate\Database\Seeder;

class CountriesSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('countries')->insert([
            ['id' => 1, 'name' => 'República Dominicana', 'code' => 'DO'],       
        ]);
    }
}
