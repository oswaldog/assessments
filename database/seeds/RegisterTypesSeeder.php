<?php

use Illuminate\Database\Seeder;

class RegisterTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('register_types')->insert([                                          
            ['id' => 1, 'name' => 'Solicitud'],
            ['id' => 2, 'name' => 'Comparable'],
            ['id' => 3, 'name' => 'Plantilla'],
        ]);
    }
}
